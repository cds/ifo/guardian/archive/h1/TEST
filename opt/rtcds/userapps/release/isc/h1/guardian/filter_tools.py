SW1_FLAGS = ["IN", "OF", "F1", "F2", "F3", "F4", "F5", "F6"]
SW1_BITS = [2, 3, 4, 6, 8, 10, 12, 14]
SW2_FLAGS = ["F7", "F8", "F9", "F10", "LT", "DC", "OT", "HD"]
SW2_BITS = [0, 2, 4, 6, 8, 9, 10, 11]


def SW1_to_flags(value):
    result = []
    count = 0
    for bit in SW1_BITS:
        if value & 2 ** bit == 1:
            result.append(SW1_FLAGS[count])
            count += 1
    return result


def flags_to_SWX(flags_list):
    sw1 = 0
    sw2 = 0
   # print("Flags list & len", flags_list, len(flags_list))
    # Catch the case where no bits are set, empty list of len 1?
    if not flags_list:
        return sw1, sw2
    if len(flags_list) == 1:
        return sw1, sw2
    else:
        for flag in flags_list:
            if flag in SW1_FLAGS:
                sw1 |= 2 ** (SW1_BITS[SW1_FLAGS.index(flag)])
            elif flag in SW2_FLAGS:
                sw2 |= 2 ** (SW2_BITS[SW2_FLAGS.index(flag)])
            else:
                raise ValueError(f"Flag passed ({flag}) not supported")

    return sw1, sw2


def SW2_to_flags(value):
    result = []
    count = 0
    for bit in SW2_BITS:
        if value & 2 ** bit == 1:
            result.append(SW2_FLAGS[count])
            count += 1
    return result
