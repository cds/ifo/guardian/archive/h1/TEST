import cdsutils
import lscparams
import gpstime
from datetime import datetime

tconvert = gpstime.tconvert
import numpy
import time
from guardian import GuardStateDecorator
import math
import awg
import awgbase
import os
from timeout_utils import call_with_timeout
import sys
import json


# Local imports for SDF test
try:
    import filter_tools
except ModuleNotFoundError:
    sys.path.append('/opt/rtcds/userapps/release/isc/h1/guardian/')
    import filter_tools


class SineMultiple(awg.Excitation):
    "multiple sine wave excitation."

    def __init__(self, chan, ampl=[0], freq=[0], phase=[0], offset=[0], start=0,
                 duration=-1, restart=-1):
        """
        Initialize a `Sine` excitation. example

            a = SineMultiple("H1:SQZ-LO_SERVO_EXC_EXC", [1, 1], [100, 1000], [0, 0], [0, 0])
            a.start()
            a.stop()

        :Parameters:
          chan : string
            Name of the excitation channel to be driven.
          ampl : float
            Amplitude in counts.
          freq : float, list
            Frequency in cycles per sec.
          phase : float, list
            Phase in radians.
          offset : float, list
            Offset in counts.
          start : float
            Start time in GPS sec (immediate if = 0).
          duration : float
            Duration in sec (infinite if < 0).
          restart : float
            Restart time in sec (no restart if <= 0).

        """
        import copy
        awg.Excitation.__init__(self, chan, start=start, duration=duration, restart=restart)

        assert (len(ampl) == len(freq) == len(phase) == len(offset))

        for i, vals in enumerate(zip(ampl, freq, phase, offset)):
            if i == 0:
                c = self.components[0]
                c.wtype = awgbase.awgSine
                c.par = vals
            else:
                c = copy.deepcopy(self.comp)
                c.wtype = awgbase.awgSine
                c.par = vals
                self.components.append(c)


##################################################
# check for lock
##################################################

def is_locked(dof):
    if dof in ['XARM_GREEN', 'YARM_GREEN']:
        '''lockedStates = ['LOCKED_NO_SLOW',
                        'LOCKED_W_SLOW_FEEDBACK',
                        'OFFLOAD_GREEN_WFS',
                        'LOCKED_NO_SLOW_NO_WFS',
                        'NO_SLOW_W_ETM_WFS',
                        'ENGAGE_WFS',
                        'LOCKED_SLOW_NO_WFS',
                        'LOCKED_RED',
                        'LOCKED_SLOW_W_ETM_WFS',
                        'LOCKED_SLOW_W_GR_WFS_PUM',
                        'NO_SLOW_W_ETM_WFS',
                        'NO_SLOW_W_GR_WFS']'''
        lockedStates = ['End Locked',
                        'Slow Engaged',
                        'Transition',
                        'Red Locked'
                        ];
        arm = dof[0]
        if (ezca.read('ALS-%s_LOCK_STATE' % arm, as_string=True) not in lockedStates
                or ezca.read('GRD-SUS_ETM%s_STATE_S' % arm, as_string=True) == 'TRIPPED'):
            return False
            ('%s arm is not locked in green' % arm)
        else:
            return True
    elif dof == 'YARM':
        return ezca['LSC-YARM_FM_TRIG_MON'] >= 1
    elif dof == 'XARM':
        return ezca['LSC-XARM_FM_TRIG_MON'] >= 1
    elif dof == 'PRX':
        return cdsutils.avg(1, 'ASC-POP_A_NSUM_OUTPUT') > lscparams.thresh['PRXY']['LOCKED']
    elif dof == 'PRY':
        return cdsutils.avg(1, 'ASC-POP_A_NSUM_OUTPUT') > lscparams.thresh['PRXY']['LOCKED']
    elif dof == 'SRX':
        return ezca['LSC-SRCL_TRIG_MON'] >= 1
    elif dof == 'SRY':
        return ezca['LSC-SRCL_TRIG_MON'] >= 1
    elif dof == 'PRMI':
        MichMon = ezca['LSC-MICH_TRIG_MON']
        log(MichMon)
        PrclMon = ezca['LSC-PRCL_TRIG_MON']
        if (ezca['LSC-MICH_TRIG_MON'] and ezca['LSC-PRCL_TRIG_MON']):
            return True
    elif dof == 'MICH_DARK':
        return cdsutils.avg(1, 'ASC-AS_A_DC_NSUM_OUTPUT') <= lscparams.thresh['MICHDARK']['LOCKED']
    elif dof == 'MICH_BRIGHT':
        return cdsutils.avg(1, 'ASC-AS_A_DC_NSUM_OUTPUT') >= lscparams.thresh['MICHBRIGHT']['LOCKED']
    elif dof == 'DRMI':
        MichMon = ezca['LSC-MICH_TRIG_MON']
        PrclMon = ezca['LSC-PRCL_TRIG_MON']
        SrclMon = ezca['LSC-SRCL_TRIG_MON']
        if (MichMon > 0.5) and (PrclMon > 0.5) and (SrclMon > 0.5):
            # We're still locked and triggered, so return True
            return True
    elif dof == 'IMC':
        if ezca['IMC-PWR_IN_OUTPUT'] < 0.01:
            # No IMC input power
            return False
        else:
            trans_pd_lock_threshold = 75  # New val after calib
            return ((ezca['IMC-MC2_TRANS_NSUM_OUTPUT'] / ezca['IMC-PWR_IN_OUTPUT'] >= trans_pd_lock_threshold)
                    and (ezca['IMC-PWR_IN_OUT16'] > 0.1))
    elif dof == 'OMC':
        return ezca['OMC-LSC_LOCK_TRIGGER_LOCKMON']
    elif dof == 'Full_IFO':
        if ezca['LSC-TR_X_NORM_INMON'] >= 500:
            return True
        else:
            return False
    elif dof == 'SQZ':
        return ezca.read('GRD-SQZ_LOCK_STATE_S', as_string=True) == 'LOCKED_SEED'
    else:
        log('checking lock of unrecognized dof')
        return None


#################################################
# check for error conditions
#################################################

def WFS_DC_centering_servos_OK(port):
    okayFlag = True  # initialze
    if 'IMC' in port:
        for ab in ['A', 'B']:
            for dof in ['PIT', 'YAW']:
                if abs(ezca['IMC-WFS_{}_DC_{}_OUTPUT'.format(ab, dof)]) > 0.7:
                    notify('IMC WFS not centered')
                    okayFlag = False
    else:
        if port == 'REFL':
            servos = [1, 2]
            optics = ['RM1', 'RM2']
        elif port == 'AS':
            servos = [3, 4]
            optics = ['OM1', 'OM2']
        elif port == 'REFL_AS':
            servos = [1, 2, 3, 4]
            optics = ['RM1', 'RM2', 'OM1', 'OM2']

        if ezca['ASC-WFS_GAIN'] < 0.1:
            notify('ASC MASTER GAIN')

        for py in ['P', 'Y']:
            for servo in servos:
                if (abs(ezca['ASC-DC%s_%s_OUTPUT' % (servo, py)]) >= ezca['ASC-DC%s_%s_LIMIT' % (servo, py)]):
                    okayFlag = False

        # If any of the OMs or RMs are saturating, we're not okay and need to do a reset.s
        for osem in ['LL', 'LR', 'UL', 'UR']:
            for opt in optics:
                if (abs(ezca['SUS-%s_M1_MASTER_OUT_%sMON' % (opt, osem)]) >= 30000):
                    okayFlag = False

    return okayFlag


def REFL_PD_OK():
    return ezca['LSC-REFL_A_LF_OUTPUT'] <= 100 / 2  # to account for HAM1 splitter June 2022


def PRXY_oscillating():
    return cdsutils.avg(1, 'LSC-POPAIR_A_LF_OUT_DQ', True)[1] >= lscparams.thresh['PRXY']['OSCILLATING']


def PSL_ready():
    flag = True
    if ezca['PSL-PMC_LOCK_ON'] != -30000:
        notify("PMC unlocked")
        flag = False
    elif ezca['PSL-PERISCOPE_A_DC_ERROR_FLAG'] != 0:
        notify("PSL periscope PD error (low light?)")
        flag = False
    if ezca['PSL-FSS_AUTOLOCK_STATE'] != 4:
        notify("FSS unlocked")
        flag = False

    return flag


def iss_ok():
    return (ezca['PSL-ISS_LOOP_STATE_OUTPUT'] > 30000)


def prg_okay(prg):
    return (ezca['LSC-PR_GAIN_OUT16'] > prg)


def cameras_ok():
    okayFlag = True
    for py in ['X', 'Y']:
        # X is horizontal/yaw, Y is vertical/pit
        for cam in ['21', '23', '25', '26', '27']:
            # Look at the cameras themselves (check dig vid cam screen for numbers vs. optics).
            # 21=ITMX, 23=ITMY, 25=ETMX, 26=BS, 27=ETMY
            if ezca['VID-CAM%s_%s' % (cam, py)] == -1.0:
                notify('camera looks like it is frozen!!!!')
                okayFlag = False
    return okayFlag


#################################################
# check for convergence
#################################################

def asc_convergence_checker(loopsList, Pit_thresholds, Yaw_thresholds):
    ConvergenceFlag = True
    for ii in range(len(loopsList)):
        if abs(ezca['ASC-{}_P_OUT16'.format(loopsList[ii])]) > Pit_thresholds[ii]:
            ConvergenceFlag = False
            log('{0} Pit not converged, thresh={1}'.format(loopsList[ii], Pit_thresholds[ii]))
        if abs(ezca['ASC-{}_Y_OUT16'.format(loopsList[ii])]) > Yaw_thresholds[ii]:
            ConvergenceFlag = False
            log('{0} Yaw not converged, thresh={1}'.format(loopsList[ii], Yaw_thresholds[ii]))
    return ConvergenceFlag


def asc_err_convergence_checker(loopsList, Pit_thresholds, Yaw_thresholds):
    ConvergenceFlag = True
    for ii in range(len(loopsList)):
        if abs(ezca['ASC-{}_P_INMON'.format(loopsList[ii])]) > Pit_thresholds[ii]:
            ConvergenceFlag = False
            log('{0} Pit error signal not converged, thresh={1}'.format(loopsList[ii], Pit_thresholds[ii]))
        if abs(ezca['ASC-{}_Y_INMON'.format(loopsList[ii])]) > Yaw_thresholds[ii]:
            ConvergenceFlag = False
            log('{0} Yaw error signal not converged, thresh={1}'.format(loopsList[ii], Yaw_thresholds[ii]))
    return ConvergenceFlag


def dither_convergence_checker(loopsList, thresholds):
    ConvergenceFlag = True
    for ii in range(len(loopsList)):
        if abs(ezca['ASC-ADS_%s_SMOOTH_INMON' % loopsList[ii]]) > thresholds[
            ii]:  # JCD 13Nov2019 look before smooth limiter
            ConvergenceFlag = False
            log('{0} error signal not converged, thresh={1}'.format(loopsList[ii], thresholds[ii]))
    return ConvergenceFlag


def alsWfs_convergence_checker(dofList, Pit_thresholds, Yaw_thresholds, arm):
    ConvergenceFlag = True
    nTest = 32
    ct = 0
    while ct < nTest:
        if ConvergenceFlag == False:
            break
        for ii in range(len(dofList)):
            Pit_value = abs(ezca['ALS-%s_WFS_DOF_%i_P_OUT16' % (arm, dofList[ii])])
            Yaw_value = abs(ezca['ALS-%s_WFS_DOF_%i_Y_OUT16' % (arm, dofList[ii])])
            if Pit_value > Pit_thresholds[ii]:
                ConvergenceFlag = False
                log('{0} Pit not converged, thresh={1}, current={2:.1f}'.format(dofList[ii], Pit_thresholds[ii],
                                                                                Pit_value))
            if Yaw_value > Yaw_thresholds[ii]:
                ConvergenceFlag = False
                log('{0} Yaw not converged, thresh={1}, current={2:.1f}'.format(dofList[ii], Yaw_thresholds[ii],
                                                                                Yaw_value))
        ct += 1
    return ConvergenceFlag


def ads_convergence_checker(loopsList, Pit_thresholds, Yaw_thresholds):
    ConvergenceFlag = True
    for ii in range(len(loopsList)):
        if abs(ezca['ASC-ADS_PIT{0}_DOF_OUT16'.format(loopsList[ii])]) > Pit_thresholds[ii]:
            ConvergenceFlag = False
            log('{0} Pit not converged, thresh={1}'.format(loopsList[ii], Pit_thresholds[ii]))
        if abs(ezca['ASC-ADS_YAW{0}_DOF_OUT16'.format(loopsList[ii])]) > Yaw_thresholds[ii]:
            ConvergenceFlag = False
            log('{0} Yaw not converged, thresh={1}'.format(loopsList[ii], Yaw_thresholds[ii]))
    return ConvergenceFlag


def ads_average_convergence_checker(dofList, PitYaw_thresholds, avgDuration=30):
    ConvergenceFlag = True
    chan_name_array = []
    for ii in range(len(dofList)):
        chan_name_array.append('ASC-ADS_PIT%s_DOF_OUT16' % (dofList[ii]))
        chan_name_array.append('ASC-ADS_YAW%s_DOF_OUT16' % (dofList[ii]))
    # Add in the "H1:"
    if not chan_name_array[0][1:3] == '1:':
        ifo = os.getenv('IFO')
        chan_name_array = ['{}:{}'.format(ifo, chan) for chan in chan_name_array]

    unconverged = []
    avgs = cdsutils.avg(avgDuration, chan_name_array)
    for channel, avg in zip(chan_name_array, avgs):
        print('%s %s' % (channel, avg))
        if abs(avg) > PitYaw_thresholds:
            unconverged.append(channel)

    if unconverged:
        log('Waiting for channels to converge: {}'.format(unconverged))
        ConvergenceFlag = False

    return ConvergenceFlag


#################################################
# check input power (backup)
#################################################
# this is what we want, we don't want to change the input sliders durring power up because that can cause locklosses
def IMC_power_adjust_func():
    # at 2.0 Watts input power the nominal gain on IMC Fast Gain should be 0 dB
    nominalInputPower = 1.85
    gain_scaler = lscparams.IMC_FASTGAIN_PowerUp_Scaler  # dB, this is to avoid saturations in the IMC Board
    calc_gain = 20.0 * math.log10(nominalInputPower / ezca['IMC-PWR_IN_OUTMON']) + gain_scaler
    calc_gain = min(calc_gain, 31)

    # it only changes the power if there is a difference of 1 dB.
    # This is intended to prevent repeated 1 dB gain changes
    #  if we are on the edge between two gain settings with the input power.

    diff_gain = calc_gain - ezca['IMC-REFL_SERVO_FASTGAIN']

    if abs(diff_gain) >= 2.0:
        R_diff_gain = round(diff_gain)
        log('power_adjust is changing IMC Servo Board FASTGAIN from {} by {}'.format(ezca['IMC-REFL_SERVO_FASTGAIN'],
                                                                                     R_diff_gain))
        ezca['IMC-REFL_SERVO_FASTGAIN'] += R_diff_gain  # Has to be +=, this was causing locklosses Dec 21, 2018

        if abs(ezca['LSC-MCL_GAIN']) > 1e-9:
            ezca['LSC-MCL_GAIN'] *= 10 ** (R_diff_gain / 20.0)

        elif abs(ezca['IMC-MCL_GAIN']) > 1e-9:
            ezca['IMC-MCL_GAIN'] *= 10 ** (R_diff_gain / 20.0)

        time.sleep(0.2)
    return


def adjust_radiation_pressure_compensation():
    loops = ['CHARD_P', 'CHARD_Y', 'DHARD_P',
             'DHARD_Y']  # , 'CSOFT_P', 'DSOFT_P'] #, 'CSOFT_Y', 'DSOFT_Y']    #adjust rpc according to circulating power in steps
    avg_circ_kW = (ezca['ASC-X_PWR_CIRC_OUT16'] + ezca['ASC-Y_PWR_CIRC_OUT16']) / 2.
    for loop in loops:
        if avg_circ_kW <= 76:
            ezca['ASC-RPC_%s_GAIN' % (loop)] = 0
        elif avg_circ_kW <= 82:
            ezca['ASC-RPC_%s_GAIN' % (loop)] = lscparams.asc_gains['RPC'][loop]['82']
        elif avg_circ_kW <= 113:
            ezca['ASC-RPC_%s_GAIN' % loop] = lscparams.asc_gains['RPC'][loop]['113']
        elif avg_circ_kW <= 126:
            ezca['ASC-RPC_%s_GAIN' % loop] = lscparams.asc_gains['RPC'][loop]['126']
        elif avg_circ_kW <= 138.6:
            ezca['ASC-RPC_%s_GAIN' % loop] = lscparams.asc_gains['RPC'][loop]['138.6']
        elif avg_circ_kW <= 164:
            ezca['ASC-RPC_%s_GAIN' % loop] = lscparams.asc_gains['RPC'][loop]['164']
        elif avg_circ_kW <= 176:
            ezca['ASC-RPC_%s_GAIN' % loop] = lscparams.asc_gains['RPC'][loop]['176']
        elif avg_circ_kW <= 195:
            ezca['ASC-RPC_%s_GAIN' % loop] = lscparams.asc_gains['RPC'][loop]['195']
        elif avg_circ_kW <= 214:
            ezca['ASC-RPC_%s_GAIN' % loop] = lscparams.asc_gains['RPC'][loop]['214']
        elif avg_circ_kW <= 233:
            ezca['ASC-RPC_%s_GAIN' % loop] = lscparams.asc_gains['RPC'][loop]['233']
        elif avg_circ_kW <= 258:
            ezca['ASC-RPC_%s_GAIN' % loop] = lscparams.asc_gains['RPC'][loop]['258']
        elif avg_circ_kW <= 330:
            ezca['ASC-RPC_%s_GAIN' % loop] = lscparams.asc_gains['RPC'][loop]['330']
        elif avg_circ_kW <= 352:
            ezca['ASC-RPC_%s_GAIN' % loop] = lscparams.asc_gains['RPC'][loop]['352']
        elif avg_circ_kW <= 365:
            ezca['ASC-RPC_%s_GAIN' % loop] = lscparams.asc_gains['RPC'][loop]['365']
        elif avg_circ_kW <= 400:
            ezca['ASC-RPC_%s_GAIN' % loop] = lscparams.asc_gains['RPC'][loop]['400']
        elif avg_circ_kW > 400:
            ezca['ASC-RPC_%s_GAIN' % loop] = lscparams.asc_gains['RPC'][loop]['400']
    return


def check_for_violins_saturation(power='low'):
    """Look back at the last 40 seconds of DCPD windows to determine
    if we can power up further.

    power - Options are 'low' or 'high' (high power is >= 60W)
    """
    gps_start = ezca['FEC-8_TIME_DIAG']
    data_dur = 30  # 30 sec of data should be plenty
    channels = ['H1:OMC-DCPD_A_WINDOW_MAX',
                'H1:OMC-DCPD_B_WINDOW_MAX',
                'H1:OMC-DCPD_A_WINDOW_MIN',
                'H1:OMC-DCPD_B_WINDOW_MIN']
    max_threshold = 2 ** 19
    # Use the timeout to avoid hung nds calls and get stuck here
    # Hoping to get the last -40 -> -10 seconds of data
    dcpd_data = timeout_utils.call_with_timeout(cdsutils.getdata, channels, data_dur, start=(gps_start - 40))
    if power == 'low':
        overall_max = numpy.max([numpy.abs(dcpd_data[0].data),
                                 numpy.abs(dcpd_data[1].data),
                                 numpy.abs(dcpd_data[2].data),
                                 numpy.abs(dcpd_data[3].data)])
        PowerupFactor = numpy.round(numpy.sqrt(lscparams.input_power['NLN'] / ezca['IMC-PWR_IN_OUT16']), 2)
        ExpectedPeakVal = overall_max * PowerupFactor
        MaxOkayPow = ezca['IMC-PWR_IN_OUT16'] / (overall_max / 2 ** 19) ** 2

        if ExpectedPeakVal > max_threshold:
            notify('EEEEEPP!  We will saturate the DCPDs with %s counts if we power up!  Current max power %s W' % (
                int(round(ExpectedPeakVal, 0)), int(numpy.floor(MaxOkayPow))))
            return False
        else:
            log('All good, go ahead and power up!')
            return True

    else:
        overall_max = numpy.max([numpy.abs(dcpd_data[0].data),
                                 numpy.abs(dcpd_data[1].data),
                                 numpy.abs(dcpd_data[2].data),
                                 numpy.abs(dcpd_data[3].data)])
        Expected_counts = 10 * (
                    overall_max - 100000) + 100000  # offset value for 40 mA is about 110,000 cts. Subtract this from overall value, multiply by 10 for whitening, add back in to determine level

        if Expected_counts > max_threshold:
            notify(
                'EEEEEPP!  ADC Counts for the DCPDs are too high.  With whitening they would be {}, larger than the {:.0f} available counts!  Cannot turn on OMC whitening! Stay here and damp violins'.format(
                    int(round(Expected_counts, 0)), max_threshold))
            return False
        elif Expected_counts > 0.6 * max_threshold:
            # Take some data to see if it has been below threshold the whole time
            # The beating seems to have a period of ~5min
            extra_data_dur = 300
            extra_data = timeout_utils.call_with_timeout(cdsutils.getdata, channels, extra_data_dur,
                                                         start=(gps_start - 310))
            if not extra_data:
                # We dont want to move on if we cant confirm we are OK
                return False
            overall_extra_max = numpy.max([numpy.abs(extra_data[0].data),
                                           numpy.abs(extra_data[1].data),
                                           numpy.abs(extra_data[2].data),
                                           numpy.abs(extra_data[3].data)])
            # Added slight fudge facter here since we were still getting saturations
            extra_expected = 10 * (overall_extra_max - 100000) + 140000
            if extra_expected > max_threshold:
                notify(
                    'ADC counts for DCPDs are still too high (would be %s according to max of last %s sec), but close to threshold. Do not yet turn on OMC whitening.' % (
                    extra_expected, extra_data_dur))
            else:
                log('Ready to turn on OMC whitening!')
                return True
        else:
            log('All good, turn on OMC whitening!')
            return True


#################################################
# SUS config
#################################################

sus_config = {
    'ALS_ONLY': {'ETMY': 'ALIGNED',
                 'ITMY': 'ALIGNED',
                 'ETMX': 'ALIGNED',
                 'ITMX': 'ALIGNED',
                 'PR2': 'ALIGNED',
                 'SR2': 'ALIGNED',
                 'PRM': 'MISALIGNED',
                 'SRM': 'MISALIGNED'},
    'PRMI_W_ALS': {'ETMY': 'ALIGNED',
                   'ITMY': 'ALIGNED',
                   'ETMX': 'ALIGNED',
                   'ITMX': 'ALIGNED',
                   'PR2': 'ALIGNED',
                   'SR2': 'ALIGNED',
                   'PRM': 'ALIGNED',
                   'SRM': 'MISALIGNED'},
    'FULL_LOCK': {'ETMY': 'ALIGNED',
                  'ITMY': 'ALIGNED',
                  'ETMX': 'ALIGNED',
                  'ITMX': 'ALIGNED',
                  'PR2': 'ALIGNED',
                  'SR2': 'ALIGNED',
                  'PRM': 'ALIGNED',
                  'SRM': 'ALIGNED'},
    'MICH': {'ETMY': 'MISALIGNED',
             'ITMY': 'ALIGNED',
             'ETMX': 'MISALIGNED',
             'ITMX': 'ALIGNED',
             'PR2': 'ALIGNED',
             'SR2': 'ALIGNED',
             'PRM': 'MISALIGNED',
             'SRM': 'MISALIGNED'},
    'PRX': {'ETMY': 'MISALIGNED',
            'ITMY': 'MISALIGNED',
            'ETMX': 'MISALIGNED',
            'ITMX': 'ALIGNED',
            'PR2': 'ALIGNED',
            'SR2': 'ALIGNED',
            'PRM': 'ALIGNED',
            'SRM': 'MISALIGNED'},
    'PRY': {'ETMY': 'MISALIGNED',
            'ITMY': 'ALIGNED',
            'ETMX': 'MISALIGNED',
            'ITMX': 'MISALIGNED',
            'PR2': 'ALIGNED',
            'SR2': 'ALIGNED',
            'PRM': 'ALIGNED',
            'SRM': 'MISALIGNED'},
    'SRX': {'ETMY': 'MISALIGNED',
            'ITMY': 'MISALIGNED',
            'ETMX': 'MISALIGNED',
            'ITMX': 'ALIGNED',
            'PR2': 'ALIGNED',
            'SR2': 'ALIGNED',
            'PRM': 'MISALIGNED',
            'SRM': 'ALIGNED'},
    'SRY': {'ETMY': 'MISALIGNED',
            'ITMY': 'ALIGNED',
            'ETMX': 'MISALIGNED',
            'ITMX': 'MISALIGNED',
            'PR2': 'ALIGNED',
            'SR2': 'ALIGNED',
            'PRM': 'MISALIGNED',
            'SRM': 'ALIGNED'},
    'SRMI': {'ETMY': 'MISALIGNED',
             'ITMY': 'ALIGNED',
             'ETMX': 'MISALIGNED',
             'ITMX': 'ALIGNED',
             'PR2': 'ALIGNED',
             'SR2': 'ALIGNED',
             'PRM': 'MISALIGNED',
             'SRM': 'ALIGNED'},
    'SR2': {'ETMY': 'MISALIGNED',
            'ITMY': 'ALIGNED',
            'ETMX': 'MISALIGNED',
            'ITMX': 'MISALIGNED',
            'PR2': 'ALIGNED',
            'SR2': 'ALIGNED',
            'PRM': 'MISALIGNED',
            'SRM': 'MISALIGNED'},
    'PRMI': {'ETMY': 'MISALIGNED',
             'ITMY': 'ALIGNED',
             'ETMX': 'MISALIGNED',
             'ITMX': 'ALIGNED',
             'PR2': 'ALIGNED',
             'SR2': 'ALIGNED',
             'PRM': 'ALIGNED',
             'SRM': 'MISALIGNED'},
    'DRMI': {'ETMY': 'MISALIGNED',
             'ITMY': 'ALIGNED',
             'ETMX': 'MISALIGNED',
             'ITMX': 'ALIGNED',
             'PR2': 'ALIGNED',
             'SR2': 'ALIGNED',
             'PRM': 'ALIGNED',
             'SRM': 'ALIGNED'},
    'XARM': {'ETMY': 'MISALIGNED',
             'ITMY': 'MISALIGNED',
             'ETMX': 'ALIGNED',
             'ITMX': 'ALIGNED',
             'PR2': 'ALIGNED',
             'SR2': 'ALIGNED',
             'PRM': 'MISALIGNED',
             'SRM': 'MISALIGNED'},
    'YARM': {'ETMY': 'ALIGNED',
             'ITMY': 'ALIGNED',
             'ETMX': 'MISALIGNED',
             'ITMX': 'MISALIGNED',
             'PR2': 'ALIGNED',
             'SR2': 'ALIGNED',
             'PRM': 'MISALIGNED',
             'SRM': 'MISALIGNED'},
    'SINGLE_BOUNCE_Y': {'ETMY': 'MISALIGNED',
                        'ITMY': 'ALIGNED',
                        'ETMX': 'MISALIGNED',
                        'ITMX': 'MISALIGNED',
                        'PR2': 'ALIGNED',
                        'SR2': 'ALIGNED',
                        'PRM': 'MISALIGNED',
                        'SRM': 'MISALIGNED'},
    'SQZ_SINGLE_BOUNCE': {'ETMY': 'MISALIGNED',
                          'ITMY': 'MISALIGNED',
                          'ETMX': 'MISALIGNED',
                          'ITMX': 'MISALIGNED',
                          'PR2': 'MISALIGNED',
                          'SR2': 'MISALIGNED',
                          'PRM': 'MISALIGNED',
                          'SRM': 'ALIGNED'},
}


def set_sus_config(config, nodes):
    log(sus_config[config])
    for sus in sus_config[config]:
        alignment = sus_config[config][sus]
        nodename = 'SUS_%s' % (sus)
        nodes[nodename] = alignment


#################################################
# DECORATORS
#################################################

def get_subordinate_watchdog_check_decorator(nodes):
    class subordinate_watchdog_check(GuardStateDecorator):
        """Check that any watchdog of a subordinate node has not tripped."""

        def pre_exec(self):
            tripped_nodes = []
            for node in nodes:
                if ('WATCHDOG_TRIPPED' in node.state):
                    tripped_nodes.append(node)

            if tripped_nodes:  # if list not empty, notify
                notify('WATCHDOG TRIPPED')

    return subordinate_watchdog_check


def unstall_nodes(nodes):
    class unstall_decorator(GuardStateDecorator):
        def pre_exec(self):
            for node in nodes.get_stalled_nodes():
                # put a check that in is in done state
                if not node.NOTIFICATION:
                    log('Unstalling ' + node.name)
                    node.revive()

    return unstall_decorator


def assert_dof_locked_gen(dofs):
    class assert_dof_locked(GuardStateDecorator):
        def pre_exec(self):
            for dof in dofs:
                if not is_locked(dof):
                    if is_locked == None:
                        notify('Unrecognized dof')

                    else:
                        if SYSTEM == 'ISC_DRMI':
                            return 'DOWN'
                        elif SYSTEM == 'ISC_LOCK':
                            if dof == 'DRMI':
                                return 'LOCKLOSS_DRMI'
                            elif dof == 'PRMI':
                                return 'LOCKLOSS_PRMI'
                            else:
                                return 'LOCKLOSS'
                        elif (SYSTEM == 'IMC_LOCK'
                              or SYSTEM == 'ALIGN_IFO'
                              or SYSTEM == 'OMC_LOCK'):
                            return 'DOWN'
                        elif (SYSTEM == 'ALS_XARM' or SYSTEM == 'ALS_YARM'):
                            return 'FAULT'
                        elif SYSTEM == 'VIOLIN_DAMPING':
                            return 'TURN_OFF_DAMPING_ALL'
                        elif SYSTEM == 'TMS_SERVO':
                            return 'TMS_SERVO_OFF'
                        elif SYSTEM == 'CAMERA_SERVO':
                            return 'DOWN'
                        else:  # ALS_COMM and ALS_DIFF Full_IFO and CAMERA_SERVO
                            return 'LOCKLOSS'

    return assert_dof_locked


class check_PSL(GuardStateDecorator):
    def pre_exec(self):
        if not PSL_ready():
            # Used by IMC node
            return 'FAULT'


def gen_check_WFS_DC(dofs):
    class check_WFS_DC(GuardStateDecorator):
        def pre_exec(self):
            for dof in dofs:
                if not WFS_DC_centering_servos_OK(dof):
                    break  # Jnote

    return check_WFS_DC


class IMC_power_adjust(GuardStateDecorator):
    def pre_exec(self):
        IMC_power_adjust_func()


class iss_checker(GuardStateDecorator):
    """Decorator that jumps to OPEN_ISS if the ISS second loop output is too large."""

    def pre_exec(self):
        if not iss_ok():
            log('1st loop is saturated, and open')
            return 'OPEN_ISS'


##################################################
# LSC/ASC input and output matrices                 #
##################################################
# LSC input matrix for the majority
intrix = cdsutils.CDSMatrix(
    'LSC-PD_DOF_MTRX',
    ramping=True,
    cols={'POP_A9I': 1,
          'POP_A9Q': 2,
          'POP_A45I': 3,
          'POP_A45Q': 4,
          'REFL_A9I': 5,
          'REFL_A9Q': 6,
          'REFL_A45I': 7,
          'REFL_A45Q': 8,
          'POPAIR_A9I': 9,
          'POPAIR_A9Q': 10,
          'POPAIR_A45I': 11,
          'POPAIR_A45Q': 12,
          'REFLAIR_A9I': 13,
          'REFLAIR_A9Q': 14,
          'REFLAIR_A45I': 15,
          'REFLAIR_A45Q': 16,
          'REFLAIR_B27I': 17,
          'REFLAIR_B27Q': 18,
          'REFLAIR_B135I': 19,
          'REFLAIR_B135Q': 20,
          'TRX': 21,
          'TRY': 22,
          'REFLSERVO_SLOW': 23,
          'ALS_COMM': 24,
          'AS_C_NSUM': 25,
          'TR_CARM': 26,
          'TR_REFL9': 27,
          'REFL_DC': 28,
          'OMC_DC': 29,
          'ASAIR_A45I': 30,
          'ASAIR_A45Q': 31,
          },
    rows={'DARM': 1,
          'CARM': 2,
          'MICH': 3,
          'PRCL': 4,
          'SRCL': 5,
          'MCL': 6,
          'XARM': 7,
          'YARM': 8,
          'REFLBIAS': 9,
          }
)

# LSC input matrix component in OMC model
intrix_OMCAS45 = cdsutils.CDSMatrix(
    'LSC-ARM_INPUT_MTRX',
    ramping=True,
    cols={'OMCDC': 1,
          'ASAIR_A45I': 2,
          'ASAIR_A45Q': 3,
          'ALS_DIFF': 4,
          'REFL_DC': 5,
          },
    rows={'DARM': 1,
          'CARM': 2,
          }
)

# LSC output matrix for the majority
outrix = cdsutils.CDSMatrix(
    'LSC-OUTPUT_MTRX',
    cols={'MICH': 1,
          'PRCL': 2,
          'SRCL': 3,
          'MCL': 4,
          'XARM': 5,
          'YARM': 6,
          'OSC1': 7,
          'OSC2': 8,
          'OSC3': 9,
          'MICHFF': 10,
          'SRCLFF': 11,
          'PRCLFF': 13,  # Yes, 13 and not 12.
          },
    rows={'ETMX': 1,
          'ETMY': 2,
          'ITMX': 3,
          'ITMY': 4,
          'PRM': 5,
          'SRM': 6,
          'BS': 7,
          'PR2': 8,
          'SR2': 9,
          'MC2': 10,
          'AO3': 11,
          }
)

darmcarm_outrix = cdsutils.CDSMatrix(
    'LSC-ARM_OUTPUT_MTRX',
    cols={'DARM': 1, 'CARM': 2},
    rows={'ETMX': 1, 'ETMY': 2, 'ITMX': 3, 'ITMY': 4}
)

# LSC trigger matrix
trigrix = cdsutils.CDSMatrix(
    'LSC-TRIG_MTRX',
    cols={'OMCDC': 1,
          'POPAIR_B_RF18_I': 2,
          'POPAIR_B_RF18_Q': 3,
          'POPAIR_B_RF90_I': 4,
          'POPAIR_B_RF90_Q': 5,
          'ASAIR_B_RF18_I': 6,
          'ASAIR_B_RF18_Q': 7,
          'ASAIR_B_RF90_I': 8,
          'ASAIR_B_RF90_Q': 9,
          'REFLAIR_A_DC': 10,
          'REFLAIR_B_DC': 11,
          'POPAIR_A_DC': 12,
          'POPAIR_B_DC': 13,
          'AS_C_NSUM': 14,
          'ASAIR_B_DC': 15,
          'REFL_A_DC': 16,
          'POP_A_DC': 17,
          'TRX': 18,
          'TRY': 19,
          'POP_A_DC_IN': 20,
          },
    rows={'DARM': 1,
          'MICH': 2,
          'PRCL': 3,
          'SRCL': 4,
          'MCL': 5,  # MCL filter bank used for CARM
          'XARM': 6,
          'YARM': 7,
          'REFLBIAS': 8,
          'CARMfiltbank': 9,
          'IFO_trig': 10,
          'SEI_trig': 11,
          }
)

# ASC matrix definitions
asc_dofs = {
    'INP1': 1,
    'INP2': 2,
    'PRC1': 3,
    'PRC2': 4,
    'MICH': 5,
    'SRC1': 6,
    'SRC2': 7,
    'DHARD': 8,
    'DHARD_B': 20,
    'DSOFT_A': 9,
    'DSOFT_B': 21,
    'CHARD': 10,
    'CHARD_B': 19,
    'CSOFT_A': 11,
    'CSOFT_B': 22,
    'DC1': 12,
    'DC2': 13,
    'DC3': 14,
    'DC4': 15,
    'DC5': 16,
    'DC6': 17,
    'DC7': 18,
}
asc_pds = {
    'AS_A_RF45_I': 1,
    'AS_A_RF45_Q': 2,
    'AS_A_RF36_I': 3,
    'AS_A_RF36_Q': 4,
    'AS_B_RF45_I': 5,
    'AS_B_RF45_Q': 6,
    'AS_B_RF36_I': 7,
    'AS_B_RF36_Q': 8,
    'REFL_A_RF9_I': 9,
    'REFL_A_RF9_Q': 10,
    'REFL_A_RF45_I': 11,
    'REFL_A_RF45_Q': 12,
    'REFL_B_RF9_I': 13,
    'REFL_B_RF9_Q': 14,
    'REFL_B_RF45_I': 15,
    'REFL_B_RF45_Q': 16,
    'REFL_A_DC': 17,
    'REFL_B_DC': 18,
    'AS_A_DC': 19,
    'AS_B_DC': 20,
    'POP_A_DC': 21,
    'POP_B_DC': 22,
    'TRX_A': 23,
    'TRX_B': 24,
    'TRY_A': 25,
    'TRY_B': 26,
    'AS_C_DC': 27,
    'IM4_TRANS': 28,
    'POP_X_I': 29,
    'POP_X_Q': 30,
    'POP_X_DC': 31,
    'AS_A_RF72_I': 32,
    'AS_A_RF72_Q': 33,
    'AS_B_RF72_I': 34,
    'AS_B_RF72_Q': 35,
}

asc_actuators = {
    'PRM': 1,
    'PR2': 2,
    'PR3': 3,
    'BS': 4,
    'ITMX': 5,
    'ITMY': 6,
    'ETMX': 7,
    'ETMY': 8,
    'SRM': 9,
    'SR2': 10,
    'SR3': 11,
    'IM1': 12,
    'IM2': 13,
    'IM3': 14,
    'IM4': 15,
    'RM1': 16,
    'RM2': 17,
    'OM1': 18,
    'OM2': 19,
    'OM3': 20,
    'TMSX': 21,
    'TMSY': 22,
    'PM1': 23,
}

# ASC input matrices
asc_intrix = {
    'PIT': cdsutils.CDSMatrix('ASC-INMATRIX_P', cols=asc_pds, rows=asc_dofs),
    'YAW': cdsutils.CDSMatrix('ASC-INMATRIX_Y', cols=asc_pds, rows=asc_dofs),
}

asc_dofs_extra = {
    'DC6': 28,
    'DC7': 29,
    'ALS_X1': 17,
    'ALS_X2': 18,
    'ALS_X3': 19,
    'ALS_Y1': 20,
    'ALS_Y2': 21,
    'ALS_Y3': 22,
    'OSC1': 23,
    'OSC2': 24,
    'OSC3': 25,
    'OSC4': 26,
    'LOCKINOSC': 27,
}
asc_dofs.update(asc_dofs_extra)  # update, to be correct for output matrix

# ASC output matrices
asc_outrix = {'PIT': cdsutils.CDSMatrix('ASC-OUTMATRIX_P', cols=asc_dofs, rows=asc_actuators),
              'YAW': cdsutils.CDSMatrix('ASC-OUTMATRIX_Y', cols=asc_dofs, rows=asc_actuators)}

# ADS matrices
asc_ads_sensors = {
    'DARM_CTRL': 1,
    'PRCL_CTRL': 2,
    'SRCL_CTRL': 3,
    'POPAIR_B_RF18_I': 4,
    'POP_A_LF': 5,
    'POPAIR_B_RF90_I': 6,
    'AS_A_NSUM': 7,
    'TR_X': 8,
    'TR_Y': 9,
}

asc_ads_pit_dofs = {
    'PIT1': 1,
    'PIT2': 2,
    'PIT3': 3,
    'PIT4': 4,
    'PIT5': 5,
    'PIT6': 6,
    'PIT7': 7,
    'PIT8': 8,
    'PIT9': 9,
    'PIT10': 10,
}

asc_ads_yaw_dofs = {
    'YAW1': 1,
    'YAW2': 2,
    'YAW3': 3,
    'YAW4': 4,
    'YAW5': 5,
    'YAW6': 6,
    'YAW7': 7,
    'YAW8': 8,
    'YAW9': 9,
    'YAW10': 10,
}

# asc ads oscillators matrix
asc_ads_oscillators = {
    'OSC1': 1,
    'OSC2': 2,
    'OSC3': 3,
    'OSC4': 4,
    'OSC5': 5,
    'OSC6': 6,
    'OSC7': 7,
    'OSC8': 8,
    'OSC9': 9,
    'OSC10': 10,
}

asc_ads_actuators_dither = {
    'PRM': 1,
    'PR2': 2,
    'PR3': 3,
    'BS': 4,
    'ITMX': 5,
    'ITMY': 6,
    'ETMX': 7,
    'ETMY': 8,
    'TMSX': 9,
    'TMSY': 10,
    'SRM': 11,
    'SR2': 12,
    'SR3': 13,
}

asc_ads_actuators_out = {
    'IM3': 1,
    'IM4': 2,
    'PRM': 3,
    'PR2': 4,
    'PR3': 5,
    'BS': 6,
    'ITMX': 7,
    'ITMY': 8,
    'ETMX': 9,
    'ETMY': 10,
    'SRM': 11,
    'SR2': 12,
    'SR3': 13,
}

asc_ads_intrix = {'PIT': cdsutils.CDSMatrix('ASC-ADS_PIT_SEN_MTRX', cols=asc_ads_sensors, rows=asc_ads_pit_dofs),
                  'YAW': cdsutils.CDSMatrix('ASC-ADS_YAW_SEN_MTRX', cols=asc_ads_sensors, rows=asc_ads_yaw_dofs)}
asc_ads_lotrix = {
    'PIT': cdsutils.CDSMatrix('ASC-ADS_LO_PIT_MTRX', cols=asc_ads_oscillators, rows=asc_ads_actuators_dither),
    'YAW': cdsutils.CDSMatrix('ASC-ADS_LO_YAW_MTRX', cols=asc_ads_oscillators, rows=asc_ads_actuators_dither)}
asc_ads_outrix = {'PIT': cdsutils.CDSMatrix('ASC-ADS_OUT_PIT_MTRX', cols=asc_ads_pit_dofs, rows=asc_ads_actuators_out),
                  'YAW': cdsutils.CDSMatrix('ASC-ADS_OUT_YAW_MTRX', cols=asc_ads_yaw_dofs, rows=asc_ads_actuators_out)}

# sqz asc matrix
sqz_asc_sensors = {
    'AS_A_42': 1,
    'AS_B_42': 2,
    'OMC_A': 3,
    'OMC_B': 4,
    'AS_A_DC': 5,
    'AS_B_DC': 6,
}
sqz_asc_dofs = {'POS': 1,
                'ANG': 2
                }
sqz_asc_actuators = {'ZM1': 1,
                     'ZM2': 2,
                     }
sqz_asc_intrix = {'PIT': cdsutils.CDSMatrix('SQZ-ASC_INMATRIX_P', cols=sqz_asc_sensors, rows=sqz_asc_dofs),
                  'YAW': cdsutils.CDSMatrix('SQZ-ASC_INMATRIX_Y', cols=sqz_asc_sensors, rows=sqz_asc_dofs)}
sqz_asc_outrix = {'PIT': cdsutils.CDSMatrix('SQZ-ASC_OUTMATRIX_P', cols=sqz_asc_sensors, rows=sqz_asc_dofs),
                  'YAW': cdsutils.CDSMatrix('SQZ-ASC_OUTMATRIX_Y', cols=sqz_asc_sensors, rows=sqz_asc_dofs)}


##################################################
# Functions to aid with in-lock alignment        #
##################################################
def alignRef():
    class alignRefClass:
        ppr3 = ezca['SUS-PR3_M1_OPTICALIGN_P_OFFSET']
        ppr2 = ezca['SUS-PR2_M1_OPTICALIGN_P_OFFSET']
        pprm = ezca['SUS-PRM_M1_OPTICALIGN_P_OFFSET']
        pim4 = ezca['SUS-IM4_M1_OPTICALIGN_P_OFFSET']
        pim3 = ezca['SUS-IM3_M1_OPTICALIGN_P_OFFSET']
        psr2 = ezca['SUS-SR2_M1_OPTICALIGN_P_OFFSET']
        psrm = ezca['SUS-SRM_M1_OPTICALIGN_P_OFFSET']
        pex = ezca['SUS-ETMX_M0_OPTICALIGN_P_OFFSET']
        pey = ezca['SUS-ETMY_M0_OPTICALIGN_P_OFFSET']
        pix = ezca['SUS-ITMX_M0_OPTICALIGN_P_OFFSET']
        piy = ezca['SUS-ITMY_M0_OPTICALIGN_P_OFFSET']
        ypr3 = ezca['SUS-PR3_M1_OPTICALIGN_Y_OFFSET']
        ypr2 = ezca['SUS-PR2_M1_OPTICALIGN_Y_OFFSET']
        yprm = ezca['SUS-PRM_M1_OPTICALIGN_Y_OFFSET']
        yim4 = ezca['SUS-IM4_M1_OPTICALIGN_Y_OFFSET']
        yim3 = ezca['SUS-IM3_M1_OPTICALIGN_Y_OFFSET']
        ysr2 = ezca['SUS-SR2_M1_OPTICALIGN_Y_OFFSET']
        ysrm = ezca['SUS-SRM_M1_OPTICALIGN_Y_OFFSET']
        yex = ezca['SUS-ETMX_M0_OPTICALIGN_Y_OFFSET']
        yey = ezca['SUS-ETMY_M0_OPTICALIGN_Y_OFFSET']
        yix = ezca['SUS-ITMX_M0_OPTICALIGN_Y_OFFSET']
        yiy = ezca['SUS-ITMY_M0_OPTICALIGN_Y_OFFSET']

    return alignRefClass()


def pr3spotmove(ref):
    ###########################
    # alog 28764
    ###########################

    # set 2
    # gppr3=0.086
    # gpprm=-13.07
    # gpim4=-305
    # gpsr2=-2.73
    # gpsrm=4.29
    # gpex=-0.7
    # gpey=-0.7
    # gpix=-0.45
    # gpiy=-0.35
    # set 1
    gppr3 = 0.07
    gpprm = -13.2
    gpim4 = -325
    gpsr2 = -2.39
    gpsrm = 3.2
    gpex = -0.7
    gpey = -0.7
    gpix = -0.45
    gpiy = -0.35
    # set 1 yaw
    gypr3 = 0.028
    gyprm = 11.07
    gyim4 = 61.22
    gysr2 = 2.22
    gysrm = 2.74
    gyex = -0.5
    gyey = +0.5
    gyix = +0.32
    gyiy = -0.31
    #
    rpprm = ezca['SUS-PRM_M1_OPTICALIGN_P_OFFSET'];
    gp = 1.0 / gpprm * (rpprm - ref.pprm)
    ezca['SUS-PR3_M1_OPTICALIGN_P_OFFSET'] = ref.ppr3 + gppr3 * gp
    # ezca['SUS-PRM_M1_OPTICALIGN_P_OFFSET']  = ref.pprm + gpprm*gp
    ezca['SUS-IM4_M1_OPTICALIGN_P_OFFSET'] = ref.pim4 + gpim4 * gp
    ezca['SUS-SR2_M1_OPTICALIGN_P_OFFSET'] = ref.psr2 + gpsr2 * gp
    ezca['SUS-SRM_M1_OPTICALIGN_P_OFFSET'] = ref.psrm + gpsrm * gp
    ezca['SUS-ETMX_M0_OPTICALIGN_P_OFFSET'] = ref.pex + gpex * gp
    ezca['SUS-ETMY_M0_OPTICALIGN_P_OFFSET'] = ref.pey + gpey * gp
    ezca['SUS-ITMX_M0_OPTICALIGN_P_OFFSET'] = ref.pix + gpix * gp
    ezca['SUS-ITMY_M0_OPTICALIGN_P_OFFSET'] = ref.piy + gpiy * gp
    #
    ryprm = ezca['SUS-PRM_M1_OPTICALIGN_Y_OFFSET'];
    gy = 1.0 / gyprm * (ryprm - ref.yprm)
    ezca['SUS-PR3_M1_OPTICALIGN_Y_OFFSET'] = ref.ypr3 + gypr3 * gy
    # ezca['SUS-PRM_M1_OPTICALIGN_Y_OFFSET']  = ref.yprm + gyprm*gy
    ezca['SUS-IM4_M1_OPTICALIGN_Y_OFFSET'] = ref.yim4 + gyim4 * gy
    ezca['SUS-SR2_M1_OPTICALIGN_Y_OFFSET'] = ref.ysr2 + gysr2 * gy
    ezca['SUS-SRM_M1_OPTICALIGN_Y_OFFSET'] = ref.ysrm + gysrm * gy
    ezca['SUS-ETMX_M0_OPTICALIGN_Y_OFFSET'] = ref.yex + gyex * gy
    ezca['SUS-ETMY_M0_OPTICALIGN_Y_OFFSET'] = ref.yey + gyey * gy
    ezca['SUS-ITMX_M0_OPTICALIGN_Y_OFFSET'] = ref.yix + gyix * gy
    ezca['SUS-ITMY_M0_OPTICALIGN_Y_OFFSET'] = ref.yiy + gyiy * gy
    #
    return True


def pr2spotmove(ref):
    pitPR3toPR2 = -9.2;
    yawPR3toPR2 = +9.2;
    pitPR3toIM4 = 56;
    yawPR3toIM4 = 11;
    pitPR3toPRM = 1.5;
    yawPR3toPRM = 2.2;
    #
    p3 = ezca['SUS-PR3_M1_OPTICALIGN_P_OFFSET']
    y3 = ezca['SUS-PR3_M1_OPTICALIGN_Y_OFFSET']
    ezca['SUS-PR2_M1_OPTICALIGN_P_OFFSET'] = ref.ppr2 + pitPR3toPR2 * (p3 - ref.ppr3)
    ezca['SUS-IM4_M1_OPTICALIGN_P_OFFSET'] = ref.pim4 + pitPR3toIM4 * (p3 - ref.ppr3)
    ezca['SUS-PRM_M1_OPTICALIGN_P_OFFSET'] = ref.pprm + pitPR3toPRM * (p3 - ref.ppr3)
    ezca['SUS-PR2_M1_OPTICALIGN_Y_OFFSET'] = ref.ypr2 + yawPR3toPR2 * (y3 - ref.ypr3)
    ezca['SUS-IM4_M1_OPTICALIGN_Y_OFFSET'] = ref.yim4 + yawPR3toIM4 * (y3 - ref.ypr3)
    ezca['SUS-PRM_M1_OPTICALIGN_Y_OFFSET'] = ref.yprm + yawPR3toPRM * (y3 - ref.ypr3)
    return True


def prmspotmove(ref):
    pitIM3toPR2 = +0.0022;
    yawIM3toPR2 = +0.0055;
    pitIM3toIM4 = -1.3;
    yawIM3toIM4 = +0.92;
    pitIM3toPRM = -0.009;
    yawIM3toPRM = +0.031;
    #
    pi3 = ezca['SUS-IM3_M1_OPTICALIGN_P_OFFSET'];
    yi3 = ezca['SUS-IM3_M1_OPTICALIGN_Y_OFFSET'];
    ezca['SUS-PR2_M1_OPTICALIGN_P_OFFSET'] = ref.ppr2 + pitIM3toPR2 * (pi3 - ref.pim3)
    ezca['SUS-IM4_M1_OPTICALIGN_P_OFFSET'] = ref.pim4 + pitIM3toIM4 * (pi3 - ref.pim3)
    ezca['SUS-PRM_M1_OPTICALIGN_P_OFFSET'] = ref.pprm + pitIM3toPRM * (pi3 - ref.pim3)
    ezca['SUS-PR2_M1_OPTICALIGN_Y_OFFSET'] = ref.ypr2 + yawIM3toPR2 * (yi3 - ref.yim3)
    ezca['SUS-IM4_M1_OPTICALIGN_Y_OFFSET'] = ref.yim4 + yawIM3toIM4 * (yi3 - ref.yim3)
    ezca['SUS-PRM_M1_OPTICALIGN_Y_OFFSET'] = ref.yprm + yawIM3toPRM * (yi3 - ref.yim3)
    return True


def take_the_shot(screenshotname: str, cam_id: str):
    """
    General function to take a screenshot
    Args:
        screenshotname: What you want to name the ss file
        cam_id: ## for the specific camera
        Save location
        /ligo/data/camera/archive/yyyy/mm/dd/
    """
    # Reset the name str to empty for each time the functions called
    # self.screenshotname = str
    # cam_id = int
    timestamp = (datetime.now())
    timestamp = timestamp.strftime('%Y-%m-%d-%H-%M')
    # Store the old name, not sure if its set somewhere? I should restore for the auto screenshot saver every hour
    old_name = f'VID-CAM{cam_id}_FILE'
    old_screensh_name = ezca[old_name]
    screensh_name_chan = f'VID-CAM{cam_id}_FILE'
    screensh_take_chan = f'VID-CAM{cam_id}_SNAP'
    # The usual directory for logs should be fine?
    ezca[screensh_name_chan] = str(screenshotname + '_' + timestamp)  # Screenshot file name
    ezca[screensh_take_chan] = 1  # Take the screenshot
    # Reset screenshot name chan
    ezca[screensh_name_chan] = old_screensh_name
    return


# from ezca import errors, SFMask
# import ezca
# ezca = ezca.Ezca()
from epics import caget, caput
# Parallel processing testing flag
ParProc_flag = False


class DUMP_SDF_DIFFS():
    """
    after down and prep_for_locking state 9 but before sdf_revert but while the tables have been switched from OBS to SAFE,
    write out to some dir, log

    TODO
    Fix channel timeouts
    ezca, /var/opt/conda/base/envs/cds/lib/python3.10/site-packages/ezca/
    Default CA timeout is 2 seconds

    """

    def __init__(self):
        self.timestamp = list(str(datetime.now()))
        self.timestamp[10] = '_'
        # It needs to create a directory for each datetime it runs
        self.path = '/ligo/logs/h1/SDF_diff_dump/%s/' % "".join(self.timestamp[2:16])
        # Set up some modules to use only in this function            
        if not os.path.exists(self.path):
            os.makedirs(self.path)

        # Hard coding a DCUID tuple generated from cdslib as said module was giving namespace issues, this should be updated when needed
        self.dcuid = [('iopcdsh8', 166), ('isiham6', 51), ('pemh8', 174), ('caley', 125), ('hpietmx', 91), ('susitmy', 30),
         ('isiham8', 167), ('pslpmc', 81), ('ioppemmx', 120), ('sussrm', 45), ('hpiham5', 60), ('asc', 19),
         ('ioppsl0', 78), ('suspr3', 37),
         ('ascimc', 20), ('hpiham2', 54), ('iopseiex', 90), ('susmc1', 34), ('sussr2', 41), ('sushtts', 21),
         ('susfc2', 168), ('iopsusb123', 28), ('iopsush7', 160), ('iscey', 96), ('isiitmx', 70), ('sqzfces', 169),
         ('hpibs', 64), ('susauxh7', 163),
         ('psliss', 79), ('omcpi', 9), ('alsey', 95), ('lsc', 10), ('calcs', 117), ('seiproc', 118), ('sustmsy', 99),
         ('ascsqzifo', 77), ('iopsush34', 38), ('susbs', 31), ('suspr2', 40), ('hpiham3', 55), ('iopseih23', 53),
         ('iopseiey', 100), ('hpiham4', 59),
         ('tcscs', 26), ('ioppemmy', 122), ('iopsush56', 43), ('ascsqzfc', 173), ('susauxb123', 112), ('susitmx', 29),
         ('pslfss', 80), ('lscaux', 11), ('calex', 124), ('hpietmy', 101), ('isiham7', 171), ('iopseih16', 48),
         ('sustmsx', 89), ('iopsush2b', 103),
         ('alsex', 85), ('susim', 104), ('iopseib1', 73), ('susprm', 36), ('iscex0', 86), ('isiitmy', 75),
         ('sussr3', 44), ('oaf', 25), ('susauxh8', 172), ('iopsush2a', 33), ('susetmx', 88), ('iopiscey', 93),
         ('iopseib2', 63), ('iopsusauxh34', 107), ('hpiitmy', 74),
         ('susauxh2', 106), ('pemex', 84), ('susmc3', 35), ('susetmypi', 127), ('iopsusauxh2', 105), ('susauxh56', 110),
         ('sqz', 76), ('isietmy', 102), ('iopoaf0', 23), ('sussqzout', 22), ('susproc', 119), ('susauxey', 116),
         ('isibs', 65), ('iopsusauxb123', 111),
         ('iopsusey', 97), ('isiham4', 61), ('susetmxpi', 126), ('pemmx', 121), ('isiham3', 57), ('bos', 66),
         ('iopsusauxey', 115), ('susfc1', 161), ('susmc2', 39), ('pemey', 94), ('iopseih45', 58), ('iopomc0', 179),
         ('susitmpi', 32), ('iopseib3', 68), ('hpiitmx', 69),
         ('iopiscex', 83), ('susetmy', 98), ('psldbb', 82), ('iopseih7', 170), ('pemmy', 123), ('isiham2', 56),
         ('iopsusauxex', 113), ('isiham5', 62), ('iopsusauxh56', 109), ('iopsusex', 87), ('sussqzin', 162),
         ('susifoout', 46), ('susauxex', 114), ('susprocpi', 71),
         ('pemcs', 24), ('susauxh34', 108), ('iopasc0', 18), ('calinj', 42), ('isietmx', 92), ('hpiham1', 49),
         ('omc', 8), ('ioplsc0', 7), ('hpiham6', 50)]

        self.model_stagnent_type = ['isi', 'hpi']
       # self.model_stagnent_type = '\t'.join(self.model_stagnent_type)

    def is_float(self, element: any) -> bool:
        # If you expect None to be passed:
        if element is None:
            return False
        try:
            float(element)
            return True
        except ValueError:
            return False

    def looks_like_enum(self, value) -> bool:
        # checks if it's all alphabet letters
        return value.isalpha()

    def is_int(self, value: any) -> bool:
        # checks if it's all digits
        return value.isdigit()

    def looks_like_filter(self, value) -> bool:
        if "." in value:
            return False
        else:
            return True

    # Convert ascii to a readable string
    def chan_val_to_string(self, chan_bytes) -> any:
        if chan_bytes is None:
            return None
        else:
            return bytes(chan_bytes).split(b'\0', 1)[0].decode('ascii')

    def CA_try_tryagain(self, chan) -> any:  # Can return float, int, or str
        # Channel access function to avoid disconnects and timeouts
        # Stop throwing ezca module not scriptable!
        counter = 0
        while counter < 4:  # It seemed like it wasn't going through each check
            if counter <= 1:
                try:
                    result = ezca[chan]
                    break
                except ezca.ca_fail_no_connect:
                    # If ezca timesout, wait a half second then try again
                    print('Ezca error')
                    counter += 1
                    time.sleep(0.5)
                except EzcaError:
                    print('IFO not specified')
                    
            if counter == 2:
                # If the sleep timer doesn't fix it, double the timeout time, I think theres a CA timeout as well
                # But I don't remember how to use it
                print("Increase ezca timeout")
                try:
                    result = ezca.read(channel=chan, timeout=4)
                    break
                except EzcaConnectError:
                    print("Anotha' Ezca error")
                    counter += 1
                except EzcaError:
                    print('IFO not specified')
                    
            if counter == 3:
                # Uses conn.iterate
                print('Multiple ezca timeouts, try cdsutil')
                result = call_with_timeout(cdsutils.getdata, 'H1:' + chan, -0.0625)
                result = result.data[0]
                if isinstance(result, None):    
                    print('CDSutils timeout, try caget')
                    # epics caget, won't work in a GRD env
                    result = caget('H1:' + chan)
                    counter += 1  # Brings it to 4 which should end the loop
                else:  # If cdu was successful
                    break
            print(type(result), result)
        return result

        # Haven't tried this function yet
        """        
    def CA_write_tryagain(self, chan, val) -> any:
        # Function to fight CA write timeouts/DCs
        counter = 0
        while counter < 2:
            if counter >= 1:
                try:
                    ezca.write(channel=chan, value=val, wait=True)
                except errors.EzcaConnectError:
                    print('ezca connect error')
                    counter =+ 1
                    pv = ezca.connect(chan)
                    pv.put(val)
                   # ezca.write(channel=chan, value=val, wait=False)
                except errors.EzcaError:
                    print('IFO not specified')
        """


    def get_diffs(self, model, ID) -> dict:
        ID = str(ID)
        model = str(model)
        expected_diffs_chan = f'FEC-{ID}_SDF_DIFF_CNT'  # chan rate = 16
        sdf_type_chan = f'FEC-{ID}_SDF_TYPE'
        page_control_chan = f"FEC-{ID}_SDF_PAGE"
        sdf_type = ezca[sdf_type_chan]

        #  DIFF_CNT chan isn't connecting with ezca consistentally
        diffs_start = gpstime.gpsnow()
        expected_diffs = int(self.CA_try_tryagain(chan=expected_diffs_chan))
        diffs_stop = gpstime.gpsnow()

        print("Time to check diffs: ", diffs_stop-diffs_start)
        print('Diffs #', expected_diffs)
        result = {}

        table_control_chan = f"FEC-{ID}_SDF_SORT"
        page_control_chan = f"FEC-{ID}_SDF_PAGE"
        search_text_chan = f'FEC-{ID}_SDF_WC_STR'
        wild_card_chan = f'FEC-{ID}_SDF_WILDCARD'
        # 4 Ezca writes for SDF table ctrl, str channels
        table_start = gpstime.gpsnow()
        ezca[table_control_chan] = "SETTING DIFFS"
        print('test2')
        time.sleep(0.0625)
        ezca[page_control_chan] = -50
        time.sleep(0.0625)
       # ezca.write(channel=search_text_chan, wait=True, value=search_txt)
        ezca[search_text_chan] = " "  # Delete any search entry
        time.sleep(0.0625)
        # 12/17 ezca write timeout
       # self.CA_write_tryagain(chan=wild_card_chan, val=wild_str)
        ezca[wild_card_chan] = "SHOW ALL"
        time.sleep(0.0625)
        table_stop = gpstime.gpsnow()

        print('Table/page duration ezca write', table_stop - table_start)
        row = 0  # Start at 0, the top, there will be as many rows as exp_diffs
        # if the channel doesn't connect, gives None, so check for None types
        diff_read_start = gpstime.gpsnow()
        if expected_diffs not in [0, None]:
            ## TODO how to speed up this loop?
            try:
                for chan in range(expected_diffs):
                    stat_base_chan = "FEC-" + ID + "_SDF_SP_STAT" + str(row)
                    stat_burt_chan = stat_base_chan + "_BURT"
                    stat_live_chan = stat_base_chan + "_LIVE"
                    stat_time_chan = stat_base_chan + "_TIME"
                # chan_list = [stat_base_chan, stat_burt_chan, stat_live_chan, 
                #              stat_time_chan]
                    time.sleep(0.0625)
                    chan_name = self.chan_val_to_string(ezca[stat_base_chan])
                    time.sleep(0.0625)  # 1/16th of a second sleep
                    setpoint = self.chan_val_to_string(ezca[stat_burt_chan])
                    time.sleep(0.0625)
                    live_val = self.chan_val_to_string(ezca[stat_live_chan])
                    time.sleep(0.0625)
                    time_val = self.chan_val_to_string(ezca[stat_time_chan])
                    # Put all the results into a tuple to check for Nones/channel timeouts
                    value_check_tup = (chan_name, setpoint, live_val, time_val)

                    # TODO: Filter are truncated so we can't call this on them
                    # chan_info = epics.cainfo(chan_name, False)
                    # type_str = re.search('type       = (.*)\n', chan_info)
                    # print(f"chan: {chan_name}, set: {setpoint}, live: {live_val}")

                    if None in value_check_tup:  # Check for None, 0s are fine
                        # If we didn't get one of the values-timeout, break out
                        print("We didn't get all the data we need, breaking out of the loop")
                        print("Tup", value_check_tup)
                        return
                    else:  # If we got the data, do the rest
                        # TODO: Support int channels? Better enum support
                        if self.looks_like_enum(setpoint) or self.looks_like_enum(live_val):
                            result[chan_name] = {'type': 'enum',
                                                 'set': setpoint,
                                                 'live': live_val,
                                                 'time_of_last_change': time_val
                                                 }
                        elif self.looks_like_filter(setpoint):
                            set_sw1, set_sw2 = filter_tools.flags_to_SWX(
                                setpoint.split(','))
                          #  print('SW 1&2', set_sw1, set_sw2)
                            live_sw1, live_sw2 = filter_tools.flags_to_SWX(
                                live_val.split(','))
                        # print(setpoint)
                        # these hex to decimal values still need to be converted to readible strings
                        # buttons = SFMask.from_swstat(SWSTAT) from sfm's cmd_decode f(x)
                            result[chan_name] = {'type': 'filter',
                                                 'sw1s_set': str((set_sw1)),
                                                 'sw2s_set': str((set_sw2)),
                                                 'sw1s_live': str((live_sw1)),
                                                 'sw2s_live': str((live_sw2)),
                                                 'time_of_last_change': str(time_val)
                                                 }
                        #    print(dir(SFMask.from_swstat(int(set_sw1))))
                        elif self.is_float(setpoint) and self.is_float(live_val):
                            result[chan_name] = {'type': 'float',
                                                 'set': float(setpoint),
                                                 'live': float(live_val)
                                                 }

                        elif self.is_int(live_val):
                            result[chan_name] = {'type': 'int',
                                                 'set': int(setpoint),
                                                 'live': int(live_val)
                                                 }

                        else:  # default back to enum
                            result[chan_name] = {'type': 'enum',
                                                 'set': setpoint,
                                                 'live': live_val,
                                                 'time_of_last_change': time_val
                                                 }
                        row += 1
                        if row == 40:  # We need to turn the page
                            print('Result len: ', len(result))
                            row = 0
                            ezca[page_control_chan] = 1
                            print("calling sleep, page change")
                            # Small sleep to let the page change
                            time.sleep(0.5)
            except UserWarning:
                print('ezca timeout, the chan might not exist anymore')
                if len(result) != expected_diffs:
                    print('The table probably changed while we were reading it, we were too slow to catch the diff')
                return result
        else:
            print('No Diffs')

        diff_read_stop = gpstime.gpsnow()
        print('Diff table read function time', diff_read_stop -
              diff_read_start)
        # same for the table, SDF_TYPE - 1: SAFE, 2: OBSERVE
        if len(result) != expected_diffs:
            #  Is expected_diffs the same as table entries?  - Yes
            print(f"WARNING: Did not get the expected number of differences(\
                  got {len(result)}, expected {expected_diffs},"
                  " values might have been changing while we read them out")
        # Check the table again, ISI and HEPI will stay in OBSERVE
        sdf_type2 = ezca[sdf_type_chan]

        for item in self.model_stagnent_type:
            if item in model:
                print(model, "ISI and HEPI models don't change sdf type")
            else:
                print('Started with sdf=: ', sdf_type, 'ended with sdf=: ',
                    sdf_type2)
        return result

    def main_run(self, k: str, v: str):
        """
        This does 8 ezca calls per loop iteration, its also a little slow. It can take a minute for the function to run
        ISC_LOCK can't handle the amount of calls, fez doesn't work anymore - threaded ezca calls
        Concurrent threading? Make the for loop faster
        https://superfastpython.com/threadpool-for-loop/#Prerequisite_prepare_your_code_before_we_make_it_concurrent
        """
        # Reset the data field to None each loop iteration for data exists checks
        json_data = None
        print(f"Working on model {k}...DCUID: {v}")
        modelname = str(k)
        v = str(v)
        expected_diffs_chan = f"FEC-{v}_SDF_DIFF_CNT"  # chan rate = 16
        # This bit feels really hacky, a way to avoid channel disconnects
        expected_diffs = int(self.CA_try_tryagain(chan=expected_diffs_chan))
        print('test1')
        # If theres no diffs don't run the slow "get diffs" function
        if expected_diffs not in (0, None):
            get_data_start = gpstime.gpsnow()
            json_data = self.get_diffs(model=k, ID=v)
            get_data_stop = gpstime.gpsnow()

            get_data_duration = float(get_data_stop - get_data_start)
            print('Get diffs function took: ', get_data_duration, ' seconds')

            if json_data is None:
                print("No data retreived, timeout")
                return
            # If we successfully get the data from get_diffs()
            else:
                if len(json_data) > 0:
                    print('len json', len(json_data))
                    with open(os.path.join(self.path, (modelname + ".json")), "w+") as my_file:
                        my_file.write(json.dumps(json_data, indent=4))
                        my_file.close()
                else:   # If json_data doesn't exist from there being no diffs, len=0
                    print("Confusion, you shouldn't have gotten here. We weren't able to get the data")
                    print(len(json_data), json_data)
        else:
            print("No diffs")
            return

    def test_run(self, model, ID):
        print(model, ID)
        print("Test")
        print(f'Model {model} with DCUID {ID}')
        return
    # function to hold outside run loop for concurrent for looping

    def run_fast(self):
    # CDSWS29 has 24 CPUs - operator ws, for chnksize divide iterable len by cpu * 4
    # Testing Parallel Processing loop
        if ParProc_flag is True:
            from multiprocessing import Pool
            dcuid = self.dcuid
            gps_start = gpstime.gpsnow()
            with Pool() as pool:
                # Use the Tuple version of DCUID, list of lists
                # divmod(124,96) ~= 2
                for result in pool.starmap(self.main_run, dcuid, chunksize=2):
                    print(result)
                pool.close()
            gps_stop = gpstime.gpsnow()
            pool_duration = gps_stop - gps_start
            print('Parallel loop exc time', pool_duration)
        else:
            # convert it to dict for the regex loop,
            # 1000x faster than iterating through tuple!
            self.dcuid = dict(self.dcuid)
            gps_start2 = gpstime.gpsnow()
            for k, v in self.dcuid.items():
                self.main_run(k=k, v=str(v))
            gps_stop2 = gpstime.gpsnow()
            reg_time = gps_stop2 - gps_start2
            print('Time for regex main_run', reg_time)

