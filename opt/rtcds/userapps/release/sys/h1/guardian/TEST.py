import sys
import time
import cdsutils.sfm
from guardian import GuardState, GuardStateDecorator, NodeManager
import lscparams
from scipy.signal import find_peaks
from scipy.stats import spearmanr
import numpy as np
# Import for camera saver
from pathlib import Path
# Imports for SDF test
import os
from datetime import datetime
# import json
from timeout_utils import call_with_timeout

# Local ISC_library imports for SDF test, moved to guardian within isc/h1
# sys.path.append('/opt/rtcds/userapps/release/isc/h1/scripts/')

from inspect import getmembers, isfunction, getmodulename
# import the whole module to est. namespace?
import ISC_library_testcopy


"""
What computer runs guardian? TEST node is the same?
 To get to the process that runs a GRD node "systemctrl status" yields
CGroup:{ user.slice
        { user-1001.slice{ 
          user@1001.service{}
         },
         { user-1010.slice{
            session-3352.scope{
           │   │ ├─2110311 sshd: guardian [priv]
           │   │ ├─2110318 sshd: guardian@pts/1
           │   │ ├─2110319 python3 -u -m guardctrl
           │   │ └─2110333 journalctl --no-pager --quiet --no-hostname --output> }
             user@1010.service{
             ...
             ...
             ...
             guardian.slice{
                guardian@TEST.service{
                1487436 guardian TEST 'path'
                1487472 guardian-worker TEST 'path'
                1488265 git cat-file --batch-check}
                }
                ...             
CGroup[user-1010.slice[user@1010.service[guardian.slice[guardian@NODE.service[]]]]]
https://medium.com/@weidagang/linux-beyond-the-basics-cgroups-f157d93bd755
h1guardian1
Sorted into controlgroups, then processes:
2 processes, guardian and a guardian-worker for each node
Does this relate to how theres a slice and a service for each as well?

How to monitor a process? See hung channel accesses? RAM increasing?
user@1010.service  is the unit/service? that runs guardian on the FE

"""


nominal = 'STATE_A'

penguin = 'Milton'

nodes = NodeManager(['ISC_LOCK', 'INIT_ALIGN'])

statecounter_list = []


def myround(x, base=60):
    """Used to round to the nearest minute to allow for
    use with minute trends.
    """
    return int(base * round(float(x) / base))


class extreme_environment(GuardStateDecorator):
    """Take a look at the environment at every so often
    and determine if we are in a state of EXTREME ENVIRONMENT
    """

    def pre_exec(self):
        # Check every 10 min on the 10s. This is instead of figuring out a timer
        if time.gmtime().tm_min % 10 == 0:
            data = call_with_timeout(cdsutils.getdata,
                                     ['H1:ISI-GND_STS_ITMY_Z_BLRMS_100M_300M',
                                      'H1:ISI-GND_STS_CS_Z_EQ_PEAK_OUTMON',
                                      'H1:PEM-EX_WIND_ROOF_WEATHER_MPH',
                                      'H1:PEM-EY_WIND_ROOF_WEATHER_MPH',
                                      # BRS BLRMS
                                      ],
                                     -600)

        # useism_data = call_with_timeout(cdsutils.getdata,
        #                                ['H1:ISI-GND_STS_ITMY_Z_BLRMS_100M_300M.mean,m-trend',
        #                                'H1:ISI-GND_STS_CS_Z_EQ_PEAK_OUTMON.mean,m-trend'],
        #                                7200,
        #                                start=myround(int(gpstime.gpsnow() - lookback)))


############################################################
# STATES
#############################################################
class IDLE(GuardState):

    def run(self):
        return True


class STATE_A(GuardState):
    ss_ISCLOCK_states = [7, 111, 590]
    ss_IA_states = [10, 20, 30, 40, 50]

    def main(self):
        # Flag to keep the state from running multiple times
        self.hasrun = False
        log('State A, main')
        log('The penguin is: {}'.format(penguin))
        self.shot = 0  # Counter, reset on LL
        # instance the class I want, do it here in main to prevent memory leaks?
        self.dsd = ISC_library_testcopy.DUMP_SDF_DIFFS()
        # Used to test stalls if manually managed
        # if penguin == 'Jerry':
        #    log('Oh no! Not Jerry')
        #    return 'STATE_B'

    def run(self):
        if ezca['GRD-ISC_LOCK_STATE_N'] == 2:  # Lockloss states to reset boolean
            return 'LOCKLOSS'
        if ezca['GRD-VIOLIN_DAMPING_STATE_N'] == 37:  # DAMP_VIOLINS_FULL_POWER
            return 'DAMP_VIOLINS_FULL_POWER'
        # Try to call ISC_library function instead of class here?
        if ezca['GRD-ISC_LOCK_STATE_N'] == 9:  # If we go through PREP_FOR_LOCKING go through this state
            # .arrived means its starting the state .done means its done ie green box?
            if nodes['ISC_LOCK'].done:  # If we stay in prep don't keep jumping
                time.sleep(30)
                return
            else:  # If the node has not arrive (finished) run DUMP_DIFFS
                # H1:FEC-##_SDF_TYPE == 2 for OBS and ==1 for SAFE
                if ezca['FEC-98_SDF_TYPE'] == 2:
                    log('Lets wait for the tables to switch')
                    time.sleep(.1)
                else: # The channel is binary, either 1 or 2
                    if not self.hasrun:
                        print('SDF run fast')
                        self.hasrun = True
                        return self.dsd.run_fast()
                return
        # The camera screenshot state logic for IA, timestamps are added to the name in the ISC-library function
        if ezca['GRD-ISC_LOCK_STATE_N'] == 7:
            # Taking out the screenshot stuff for now since the _FILE channels don't exist anymore
            pass 
        """
            # Initial Alignment, fully offloaded node -done
            if ezca['GRD-INIT_ALIGN_STATE_N'] == 10 and nodes['INIT_ALIGN'].done:
                # Green arms, fully offloaded node -done
                ISC_library_testcopy.take_the_shot('IA_GREEN_OFFL_ALS_X', '04')
                ISC_library_testcopy.take_the_shot('IA_GREEN_OFFL_ALS_Y', '05')
            if ezca['GRD-INIT_ALIGN_STATE_N'] == 20 and nodes['INIT_ALIGN'].done:
                # input align, fully offloaded node -done
                ISC_library_testcopy.take_the_shot('IA_INPUT_OFFL_AS_AIR', 16)
            if ezca['GRD-INIT_ALIGN_STATE_N'] == 30 and nodes['INIT_ALIGN'].done:
                # PRC, fully offloaded node -done
                ISC_library_testcopy.take_the_shot('IA_PRC_OFFL_AS_AIR', 16)
                ISC_library_testcopy.take_the_shot('IA_PRC_OFFL_PRM', 14)
            if ezca['GRD-INIT_ALIGN_STATE_N'] == 40 and nodes['INIT_ALIGN'].done:
                # MICH bright, fully offloaded node -done
                ISC_library_testcopy.take_the_shot('IA_MICH_B_OFFL_AS_AIR', 16)
                ISC_library_testcopy.take_the_shot('IA_MICH_B_OFFL_BS', 26)
            if ezca['GRD-INIT_ALIGN_STATE_N'] == 50 and nodes['INIT_ALIGN'].done:
                # SRC, fully offloaded node -done
                ISC_library_testcopy.take_the_shot('IA_SRC_OFFL_AS_AIR', 16)
                ISC_library_testcopy.take_the_shot('IA_SRC_OFFL_SRM', 17)
        if self.shot < 1:
            if ezca['GRD-ISC_LOCK_STATE_N'] == 111 and nodes['ISC_LOCK'].done:
                self.shot += 1
                return ISC_library_testcopy.take_the_shot('AS_AIR_DRMI-OFFL', 16)
            if ezca['GRD-ISC_LOCK_STATE_N'] == 52 and nodes['ISC_LOCK'].done:
                return ISC_library_testcopy.take_the_shot('AS_AIR_PRMI-OFFL', 16)
        elif self.shot == 1:
            if ezca['GRD-ISC_LOCK_STATE_N'] == 508 and nodes['ISC_LOCK'].done:
                return
            if ezca['GRD-ISC_LOCK_STATE_N'] == 590 and nodes['ISC_LOCK'].done:
                # SQZT0 SHG trans
                return ISC_library_testcopy.take_the_shot('SQZT0_SHG_CLOSEBEAMD', 18)
            if ezca['GRD-ISC_LOCK_STATE_N'] == 600 and nodes['ISC_LOCK'].done:
                ISC_library_testcopy.take_the_shot('SQZT0_SHG_TRANS_NLN', 18)
                ISC_library_testcopy.take_the_shot('AS_AIR_NLN', 16)
                return
        """

class STATE_B(GuardState):

    def main(self):
        log('State B, main')
        log('Changing the penguin')
        global penguin
        penguin = 'Jerry'
        log(penguin)

    def run(self):
        return True


def diff_by_indices(lst, indices) -> list:
    result = []
    for i in range(len(indices) - 1):
        idx1, idx2 = indices[i], indices[i + 1]
        result.append(lst[idx2] - lst[idx1])
    return result

class DAMP_VIOLINS_FULL_POWER(GuardState):
    index = 37
    request = True

    # @ISC_library.assert_dof_locked_gen(['Full_IFO'])
    # This wrapper returns a LOCKLOSS jump state which this node does not have which throws it into error
    def main(self):
        self.timer['monitor pause'] = 6 * 60
        self.wd_flag = {}
        self.dcpd_dict = {}
        self.PD = ['A', 'B']
        self.qual = ['MIN', 'MAX']
        for item in self.PD:
            self.dcpd_dict[item] = {}
            for thing in self.qual:
                self.dcpd_dict[item][thing] = [ezca[f'OMC-DCPD_{item}_WINDOW_{thing}']]
        self.modes_off = []
        self.susList = ['ITMX', 'ITMY', 'ETMX', 'ETMY']
        self.timer['1min_check'] = 60  # was 300
        for sus in self.susList:
            self.wd_flag[sus] = False
        self.mode_dict = {}
        for sus in self.susList:
            self.mode_dict[sus] = {}
            for mode in lscparams.vio_settings[sus]:
                self.mode_dict[sus][mode] = {
                    'starting_mon': ezca['SUS-%s_L2_DAMP_MODE%s_RMSLP_LOG10_OUTMON' % (sus, mode)],
                    'gain_mon': ezca['SUS-%s_L2_DAMP_MODE%s_GAIN' % (sus, mode)],
                    'inUse_mon': [],
                    'damping_off': False}
                # If the monitor starts too low (so that glitches will make it increase), instead start with a monitor level of 1
                if ezca['SUS-%s_L2_DAMP_MODE%s_RMSLP_LOG10_OUTMON' % (sus, mode)] <= 2:
                    self.mode_dict[sus][mode]['inUse_mon'].append(2)
                else:
                    self.mode_dict[sus][mode]['inUse_mon'].append(
                        ezca['SUS-%s_L2_DAMP_MODE%s_RMSLP_LOG10_OUTMON' % (sus, mode)])


    # @ISC_library.assert_dof_locked_gen(['Full_IFO'])
    def run(self):
        if ezca['GRD-VIOLIN_DAMPING_STATE_N'] != 37:  # DAMP_VIOLINS_FULL_POWER
            return 'STATE_A'
        if self.timer['monitor pause']:
            # Check for WD trips
            for sus in self.susList:
                if not (ezca['SUS-%s_BIO_L2_MON' % (sus)] == 26214 or ezca['SUS-%s_BIO_L2_MON' % (sus)] == 13107 or ezca['SUS-%s_BIO_L2_MON' % (sus)] == 8738):
                    # If L2 WD is tripped, turn off damping
                    self.wd_flag[sus] = True
                else:
                    if self.wd_flag[sus] == True:
                        # L2 WD was tripped but has been reset; turn damping back on
                        for mode in lscparams.vio_settings[sus]:  # Code taken from ENGAGE_DAMPING states
                            # check that the monitor isn't so high that we should engage damping with low gain
                            if ezca['SUS-%s_L2_DAMP_MODE%s_RMSLP_LOG10_OUTMON' % (sus, mode)] > lscparams.vio_mon['Rung_up']:
                                log('MODE%s on %s is rung up so much we are reducing the gain we use' % (mode, sus))
                                nominal_gain = lscparams.vio_settings[sus][str(mode)]['gain']
                                # how many orders of magnitude is the mode rung up above what we expect? assuming 5 is the monitor level for which the gain is set.
                                amp = 10 ** (ezca['SUS-%s_L2_DAMP_MODE%s_RMSLP_LOG10_OUTMON' % (sus, mode)] - lscparams.vio_mon['Nominal'])
                                calc_gain = round(nominal_gain / amp, 3)
                                ezca.get_LIGOFilter('SUS-%s_L2_DAMP_MODE%s' % (sus, mode)).ramp_gain(calc_gain, 10, wait=False)
                            # check based on the monitor if this mode is rung up enough to damp in RF DARM
                            elif ezca['SUS-%s_L2_DAMP_MODE%s_RMSLP_LOG10_OUTMON' % (sus, mode)] > lscparams.vio_mon['DC_noise_floor']:
                                ezca.get_LIGOFilter('SUS-%s_L2_DAMP_MODE%s' % (sus, mode)).ramp_gain(lscparams.vio_settings[sus][str(mode)]['gain'], 10, wait=False)
                            else:
                                log('MODE%s on %s is not rung up enough to damp' % (mode, sus))
                    self.wd_flag[sus] = False
                # Turn off damping if WD tripped
                if self.wd_flag[sus]:
                    notify('WD tripped on %s, turning off damping' % (sus))
                    # ezca.get_LIGOFilter('SUS-%s_L2_DAMP_MODE%s'%(sus, mode)).ramp_gain(0, 5, wait=False)
                    log('This is where I WOULD turn off damping on %s' % (sus))
                    
                """
                To see if the violins are ringing up, take mode data every minute. After 25 minutes do some stats
                Calc the Spearman correlation coefficients, if its high then find all the peaks and see if they're
                monotonic (constantly increasing). If they are, take some more data and notify.
                """
            # H1:OMC-DCPD_{A,B}_WINDOW_{MIN,MAX}
            if self.timer['1min_check']:  # Should we do a specific check for known oscillatory modes? ITMY 5/6 EY 18
                for sus in self.susList:
                    for mode in lscparams.vio_settings[sus]:
                        # Add current mode level to list
                        self.mode_dict[sus][mode]['inUse_mon'].append(
                            ezca['SUS-%s_L2_DAMP_MODE%s_RMSLP_LOG10_OUTMON' % (sus, mode)])

                        self.dcpd_dict['A']['MIN'].append(ezca['OMC-DCPD_A_WINDOW_MIN'])
                        self.dcpd_dict['A']['MAX'].append(ezca['OMC-DCPD_A_WINDOW_MAX'])
                        self.dcpd_dict['B']['MIN'].append(ezca['OMC-DCPD_B_WINDOW_MIN'])
                        self.dcpd_dict['B']['MAX'].append(ezca['OMC-DCPD_B_WINDOW_MAX'])
                        # Don't turn off damping if mode is low enough
                        if ezca['SUS-%s_L2_DAMP_MODE%s_RMSLP_LOG10_OUTMON' % (sus, mode)] >= 2:
                            # Check the dcpds every minute, can't see slow ringups
                            if len(self.dcpd_dict['A']['MIN']) % 2 == 0:
                              #  print('DCPD diff: ', self.dcpd_dict['A']['MIN'][-1] - self.dcpd_dict['A']['MIN'][-2])
                                for PD in self.dcpd_dict.keys():
                                    # The usual "breathing" looks to be around 3000cts, min to max
                                    if (self.dcpd_dict[PD]['MIN'][-1] - self.dcpd_dict[PD]['MIN'][-2]) > 1500\
                                    and (self.dcpd_dict[PD]['MAX'][-1] - self.dcpd_dict[PD]['MAX'][-2]) > 1500:
                                        log('The DCPD MIN/MAX may be diverging, investigate further')
                                        log('%s MIN has changed by %s' % (PD, {self.dcpd_dict[PD]['MIN'][-1] - self.dcpd_dict[PD]['MIN'][-2]}))
                                        log('%s MAX has changed by %s' % (PD, {self.dcpd_dict[PD]['MAX'][-1] - self.dcpd_dict[PD]['MAX'][-2]}))
                                    # Glitch check
                                    if (abs(self.dcpd_dict[PD]['MIN'][-1] - self.dcpd_dict[PD]['MIN'][-2])) > 10000\
                                    and (abs(self.dcpd_dict[PD]['MAX'][-1] - self.dcpd_dict[PD]['MAX'][-2])) > 10000:
                                        log('Looks like a glitch seen in the DCPDs')
                                    # Clear the values of the dict
                                    del self.dcpd_dict[PD]['MAX'][:]
                                    del self.dcpd_dict[PD]['MIN'][:]
                            # 1 min check
                            if len(self.mode_dict[sus][mode]['inUse_mon']) % 2 == 0:
                                print('1min check', len(self.mode_dict[sus][mode]['inUse_mon']))
                                if self.mode_dict[sus][mode]['inUse_mon'][-1] - self.mode_dict[sus][mode]['inUse_mon'][-2] > 1.5:
                                    diff = self.mode_dict[sus][mode]['inUse_mon'][-1] - self.mode_dict[sus][mode]['inUse_mon'][-2]
                                    log('%s mode %s is growing over the past 1 minute! Turning off gain...' % (sus, mode))
                                    log('Current monitor level is %s' % (ezca['SUS-%s_L2_DAMP_MODE%s_RMSLP_LOG10_OUTMON' % (sus, mode)]))
                                    log('The mode has grown by %s' %(diff))
                                    # ezca['SUS-%s_L2_DAMP_MODE%s_GAIN'%(sus, mode)] = 0
                                    log('This is where I WOULD turn gain for %s mode %s to zero' % (sus, mode))
                                    self.mode_dict[sus][mode]['gain_mon'] = 0
                                    self.mode_dict[sus][mode]['damping_off'] = True
                            # 5 min check
                            if len(self.mode_dict[sus][mode]['inUse_mon']) % 6 == 0:
                                print('5min', len(self.mode_dict[sus][mode]['inUse_mon']))
                                if self.mode_dict[sus][mode]['inUse_mon'][-1] - self.mode_dict[sus][mode]['inUse_mon'][-5] > 1:
                                    diff = self.mode_dict[sus][mode]['inUse_mon'][-1] - self.mode_dict[sus][mode]['inUse_mon'][-5]
                                    log('%s mode %s is growing over the past 5 minutes! Turning off gain...' % (sus, mode))
                                    log('Current monitor level is %s' % (ezca['SUS-%s_L2_DAMP_MODE%s_RMSLP_LOG10_OUTMON' % (sus, mode)]))
                                    log('The mode has grown by %s' %(diff))
                                    # ezca['SUS-%s_L2_DAMP_MODE%s_GAIN'%(sus, mode)] = 0
                                    log('This is where I WOULD turn gain for %s mode %s to zero' % (sus, mode))
                                    self.mode_dict[sus][mode]['gain_mon'] = 0
                                    self.mode_dict[sus][mode]['damping_off'] = True
                            # 10 min check
                            if len(self.mode_dict[sus][mode]['inUse_mon']) % 11 == 0:
                                print('10min check', len(self.mode_dict[sus][mode]['inUse_mon']))
                                coef, p_val = spearmanr(np.arange(0, len(self.mode_dict[sus][mode]['inUse_mon']),1),
                                                    self.mode_dict[sus][mode]['inUse_mon'])
                               # print(sus, 'mode', mode, 'Coef', coef, 'p-val spearman', p_val)
                               # peaks_idx, peak_dict = find_peaks(self.mode_dict[sus][mode]['inUse_mon'], prominence=0.4)
                                if coef >= 0.4 and p_val <= 0.05:
                                    print('Coef', coef, 'p_val', p_val)
                                    print(f'{sus} {mode} might be ringing up, please investigate further')

                                if self.mode_dict[sus][mode]['inUse_mon'][-1] - self.mode_dict[sus][mode]['inUse_mon'][-9] > 0.3:
                                    diff = self.mode_dict[sus][mode]['inUse_mon'][-1] - self.mode_dict[sus][mode]['inUse_mon'][-9]
                                    log('%s mode %s is growing over the past 10 minutes! Turning off gain...' % (sus, mode))
                                    log('Current monitor level is %s' % (ezca['SUS-%s_L2_DAMP_MODE%s_RMSLP_LOG10_OUTMON' % (sus, mode)]))
                                    log('The mode has grown by %s' %(diff))
                                    # ezca['SUS-%s_L2_DAMP_MODE%s_GAIN'%(sus, mode)] = 0
                                    log('This is where I WOULD turn gain for %s mode %s to zero' % (sus, mode))
                                    self.mode_dict[sus][mode]['gain_mon'] = 0
                                    self.mode_dict[sus][mode]['damping_off'] = True
                            # 25 min check, should we still do the 45 minute check?
                            # Take some stats, spearman corr factors, -1 to 1, Or do peak analysis?
                            # +/- 0.8 - 1.0 very strong inc/dec
                            # 0.6 - 0.79 Strong
                            # 0.4 - 0.59 Moderate
                            # 0.2 - 0.399 Weak
                            # 0.0 - 0.199 Very weak
                            if len(self.mode_dict[sus][mode]['inUse_mon']) % 26 == 0:
                                print('25min check', len(self.mode_dict[sus][mode]['inUse_mon']))
                                # -1 is decreasing, +1 is increasing - Spearman coefficients
                                coef, p_val = spearmanr(np.arange(0, len(self.mode_dict[sus][mode]['inUse_mon']),1),
                                                    self.mode_dict[sus][mode]['inUse_mon'])
                                print(sus, 'mode', mode, 'Coef', coef, 'p-val spearman', p_val)
                                peaks_idx, peak_dict = find_peaks(self.mode_dict[sus][mode]['inUse_mon'], prominence=0.4)
                                # If theres a strong correlation and small chance of it being random (95% confidence interval)
                                if coef >= 0.4 and p_val <= 0.05:
                                    peak_diff = diff_by_indices((self.mode_dict[sus][mode]['inUse_mon']), peaks_idx)
                                    print(peak_diff)
                                    # Test for monotonic, constantly increasing. If the above coef is low this probably would fail
                                    if np.all(np.array(peak_diff) > 0 ):
                                        print(f'{sus} {mode} seems to be constantly increasing, investigate further')
                                        self.mode_dict[sus][mode]['gain_mon'] = 0
                                        self.mode_dict[sus][mode]['damping_off'] = True
                                        return
                                    for peak, idx in enumerate(peaks_idx):
                                        print(self.mode_dict[sus][mode]['inUse_mon'][peak])

                                    if self.mode_dict[sus][mode]['inUse_mon'][-1] - self.mode_dict[sus][mode]['inUse_mon'][-24] > 0.1:
                                        diff = self.mode_dict[sus][mode]['inUse_mon'][-1] - self.mode_dict[sus][mode]['inUse_mon'][-9]
                                        log('%s mode %s is growing over the past 25 minutes! Turning off gain...' % (sus, mode))
                                        log('Current monitor level is %s' % (ezca['SUS-%s_L2_DAMP_MODE%s_RMSLP_LOG10_OUTMON' % (sus, mode)]))
                                        log('The mode has grown by %s' %(diff))
                                        # ezca['SUS-%s_L2_DAMP_MODE%s_GAIN'%(sus, mode)] = 0
                                        log('This is where I WOULD turn gain for %s mode %s to zero' % (sus, mode))
                                        self.mode_dict[sus][mode]['gain_mon'] = 0
                                        self.mode_dict[sus][mode]['damping_off'] = True
                                    # 45 min check
                                    if len(self.mode_dict[sus][mode]['inUse_mon']) % 46 == 0:
                                        print('45min check', len(self.mode_dict[sus][mode]['inUse_mon']))
                                        if self.mode_dict[sus][mode]['inUse_mon'][-1] - self.mode_dict[sus][mode]['inUse_mon'][-44] > 0.1:
                                            diff = self.mode_dict[sus][mode]['inUse_mon'][-1] - self.mode_dict[sus][mode]['inUse_mon'][-9]
                                            log('%s mode %s is growing over the past 45 minutes! Turning off gain...' % (sus, mode))
                                            log('Current monitor level is %s' % (ezca['SUS-%s_L2_DAMP_MODE%s_RMSLP_LOG10_OUTMON' % (sus, mode)]))
                                            log('The mode has grown by %s' %(diff))
                                            # ezca['SUS-%s_L2_DAMP_MODE%s_GAIN'%(sus, mode)] = 0
                                            log('This is where I WOULD turn gain for %s mode %s to zero' % (sus, mode))
                                            self.mode_dict[sus][mode]['gain_mon'] = 0
                                            self.mode_dict[sus][mode]['damping_off'] = True
                                        # Delete the entries, so the dict doesn't get too unwieldy
                                        del self.mode_dict[sus][mode]['inUse_mon'][:(len(self.mode_dict[sus][mode]['inUse_mon']))]
                                else:
                                    del self.mode_dict[sus][mode]['inUse_mon'][:(len(self.mode_dict[sus][mode]['inUse_mon']))]
  
                # Wait this long between checking modes
                self.timer['1min_check'] = 60
            """ 
            Commented out 01/25/25 RC
            if self.timer['5min_check']:
                for sus in self.susList:
                    for mode in lscparams.vio_settings[sus]:
                        # Add current mode level to list
                        self.mode_dict[sus][mode]['inUse_mon'].append(
                            ezca['SUS-%s_L2_DAMP_MODE%s_RMSLP_LOG10_OUTMON' % (sus, mode)])
                        # Don't turn off damping if mode is low enough
                        if ezca['SUS-%s_L2_DAMP_MODE%s_RMSLP_LOG10_OUTMON' % (sus, mode)] >= 2:
                            # 5min check
                            if self.mode_dict[sus][mode]['inUse_mon'][-1] - self.mode_dict[sus][mode]['inUse_mon'][-2] > 1:  # FIXME: tune 5min threshold
                                log('%s mode %s is growing over the past 5 minutes! Turning off gain...' % (sus, mode))
                                log('Current monitor level is %s' % (ezca['SUS-%s_L2_DAMP_MODE%s_RMSLP_LOG10_OUTMON' % (sus, mode)]))
                                # ezca['SUS-%s_L2_DAMP_MODE%s_GAIN'%(sus, mode)] = 0
                                log('This is where I WOULD turn gain for %s mode %s to zero' % (sus, mode))
                                self.mode_dict[sus][mode]['gain_mon'] = 0
                                self.mode_dict[sus][mode]['damping_off'] = True
                            # 45min check
                            if len(self.mode_dict[sus][mode]['inUse_mon']) > 9:
                                # log('Start 45min check for %s mode %s'%(sus, mode))
                                if self.mode_dict[sus][mode]['inUse_mon'][-1] - self.mode_dict[sus][mode]['inUse_mon'][-10] > 0:  # FIXME: tune 45min threshold
                                    log('%s mode %s is growing over the past 45 minutes! Turning off gain...' % (sus, mode))
                                    log('Current monitor level is %s' % (ezca['SUS-%s_L2_DAMP_MODE%s_RMSLP_LOG10_OUTMON' % (sus, mode)]))
                                    # ezca['SUS-%s_L2_DAMP_MODE%s_GAIN'%(sus, mode)] = 0
                                    log('This is where I WOULD turn gain for %s mode %s to zero' % (sus, mode))
                                    self.mode_dict[sus][mode]['gain_mon'] = 0
                                    self.mode_dict[sus][mode]['damping_off'] = True
                                del self.mode_dict[sus][mode]['inUse_mon'][:(len(self.mode_dict[sus][mode]['inUse_mon']) - 9)]
                # Wait this long between checking modes
                self.timer['5min_check'] = 300
            """
            for sus in self.susList:
                for mode in lscparams.vio_settings[sus]:
                    # If gain changed, someone is probably damping it manually
                    # Log the change and clear the list
                    if ezca['SUS-%s_L2_DAMP_MODE%s_GAIN' % (sus, mode)] != self.mode_dict[sus][mode]['gain_mon']:
                        log('%s %s gain has changed from %s to %s' % (sus, mode, self.mode_dict[sus][mode]['gain_mon'], ezca['SUS-%s_L2_DAMP_MODE%s_GAIN' % (sus, mode)]))
                        if self.mode_dict[sus][mode]['gain_mon'] == 0:
                            # Gain was turned on
                            self.mode_dict[sus][mode]['damping_off'] = False
                            if '%s %s' % (sus, mode) in self.modes_off:
                                self.modes_off.remove('%s %s' % (sus, mode))
                        if ezca['SUS-%s_L2_DAMP_MODE%s_GAIN' % (sus, mode)] == 0:
                            # Gain was turned off
                            self.mode_dict[sus][mode]['damping_off'] = True
                        self.mode_dict[sus][mode]['gain_mon'] = ezca['SUS-%s_L2_DAMP_MODE%s_GAIN' % (sus, mode)]
                        self.mode_dict[sus][mode]['inUse_mon'] = []
                        self.mode_dict[sus][mode]['inUse_mon'].append(ezca['SUS-%s_L2_DAMP_MODE%s_RMSLP_LOG10_OUTMON' % (sus, mode)])

                    if self.mode_dict[sus][mode]['damping_off']:
                        if '%s %s' % (sus, mode) not in self.modes_off:
                            self.modes_off.append('%s %s' % (sus, mode))
            if self.modes_off:
                notify('Modes turned off: %s' % (self.modes_off))

        return  # True, removed as it was making it jump back and forth from A to DAMP


class LOCKLOSS(GuardState):
    index = 15
    request = False
    goto = True
    # Reset the camera_saver boolean
    hasrun = False

    def run(self):
       # if ezca['GRD-ISC_LOCK_STATE_N'] == 2:
            return 'STATE_A'


edges = [
    ('STATE_A', 'STATE_B'),
    ('STATE_A', 'LOCKLOSS'),
    ('LOCKLOSS', 'STATE_A'),
    ('INIT', 'STATE_A'),
    # ('STATE_A', 'STATE_B'),
    # ('STATE_B', 'STATE_A'),
    ('DAMP_VIOLINS_FULL_POWER', 'STATE_A'),
    ('IDLE', 'DAMP_VIOLINS_FULL_POWER'),
    ('DAMP_VIOLINS_FULL_POWER', 'IDLE'),
    ('STATE_A', 'IDLE'),
    ('IDLE', 'STATE_A'),
]
