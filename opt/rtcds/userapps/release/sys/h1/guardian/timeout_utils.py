import signal
import functools

class Timeout(Exception):
    pass

# Based off https://stackoverflow.com/questions/2281850/timeout-function-if-it-takes-too-long-to-finish
# We need a timer to stop hung nds calls. This is guardian specific due to the use of log
def timeout(seconds=10, timeout_result=False):
    def decorator(func):
        #def _handle_timeout():
        def _handle_timeout(signum, frame):
            log('Timer expired')
            raise Timeout()

        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            signal.signal(signal.SIGALRM, _handle_timeout)
            signal.alarm(seconds)
            try:
                result = func(*args, **kwargs)
            except Timeout:
                return timeout_result
            finally:
                signal.alarm(0)
            return result
        
        return wrapper
    return decorator


class TimeoutOf():
    def __init__(self, seconds=10, error_message='Timer expired', timeout_result=False):
        self.seconds = seconds
        self.err_msg = error_message
        self.timeout_result = timeout_result

    def __enter__(self):
        signal.signal(signal.SIGALRM, self.handle_timeout)
        signal.alarm(self.seconds)

    def handle_timeout(self, signum, frame):
        log(self.err_msg)
        raise Timeout

    def __exit__(self, exc_type, exc_val, exc_tb):
        signal.alarm(0)


def call_with_timeout(func, *args, attempts_allowed=3, seconds_timeout=10, **kwargs):
    """
    Function wrapper to help avoid hung nds calls.

    func - name of function
    *args - any arguments needed to be passed to the func
    attempts_allowed - number of tries before giving up
    seconds_timeout - int seconds before calling this attempt a fail
        (CAUTION! There could be race conditions youll need to think about here)
    **kwargs - any key word arguments needed for func


    Example:

    import cdsutils

    popdata_prmi = call_with_timeout(cdsutils.getdata, 'LSC-POPAIR_B_RF90_I_ERR_DQ', -60)
        # This conditional handles None data returned
        if popdata_prmi.data:
            print(popdata_prmi.data.max())
    """

    counter = 0
    while counter < attempts_allowed:
        try:
            with TimeoutOf(seconds=seconds_timeout):
                val = func(*args, **kwargs)
                return val
        except Timeout:
            if counter < attempts_allowed - 1:
                log('Couldnt do it, trying again (attempt: {})'.format(counter))
                counter += 1
            else:
                log('Still couldnt do it, giving up')
                counter += 1
                break
    return False